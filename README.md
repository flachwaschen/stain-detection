# Stain Detection
Software für die Erkennung von Flecken mittels neuronalen Netzen der Salienzerkennung.

## Installation
Die Software ist in der Programmiersprache [Python](https://www.python.org/) geschrieben.
Dementsprechend wird ein entsprechender Interpreter benötigt.
Außerdem nutzt die Software andere Python-Bibliotheken, wie [PyTorch](https://pytorch.org/), [NumPy](https://numpy.org/) und [OpenCV](https://opencv.org/).
Um diese zu installieren wird der Paketmanager [PIP](https://pip.pypa.io/) genutzt.
Des Weiteren ist es in Python üblich die Abhängigkeiten einzelner Projekte in virtuellen Umgebungen zu trennen.
Dafür kommt hier [venv](https://docs.python.org/3/library/venv.html) zum Einsatz.

1. Installiere Interpreter, Paketmanager und Manager für virtuelle Umgebungen: `sudo apt install python3 python3-pip python3-venv`
2. Erstelle eine virtuelle Umgebung: `python -m venv venv`
3. Laden der virtuellen Umgebung: `source venv/bin/activate`
4. Installation der Software: `./install.sh`

Mit dem Script `install.sh` wird die Software für die Fleckerkennung und deren Abhängigkeiten in der virtuellen Umgebung installiert.
Das bedeutet auch, dass man vor dem verwenden der Software immer sicherstellen muss, dass die virtuelle Umgebung geladen ist.

## Nutzung
Alle unten aufgelisteten Befehle können mit dem Argument `-h/--help` gestartet werden, um einen genauen Einblick in die möglichen Argumente zu bekommen.

* Datensatz vorverarbeiten:
  * `python -m stain_detection.dataset.split`
  * `python -m stain_detection.dataset.patch_stain_based`
  * `python -m stain_detection.dataset.patch_sliding_window`
* Training: `python -m stain_detection.train`
* Salienzkarten berechnen: `python -m stain_detection.compute_sams`
* Metriken Berechnen und Visualisieren:
  * `python -m stain_detection.measures.compute_stats`
  * `python -m stain_detection.vis.rdr_curve`

Also Dokumentation kann auch ein Blick in eine [vorab Version meiner Dissertation](https://uni-bielefeld.sciebo.de/s/AzrFNb7F5YjmTxm) nützlich sein.

### Training
Der folgende Abschnitt beschreibt beispielhaft, wie das Vorbereiten, die Durchführung und die Evaluation eines Trainings durchgeführt werden können:

* Erstelle einen Ordner für alle Daten des Trainings: `mkdir train`
* Kopiere den Three-Illuminations Datensatz (s.u) nach `train/three-illuminations`
* Teile die Bilder im Datensatz nach Wäschestück ein: `mkdir -p train/split && python -m stain_detection.dataset.split --labels_dir train/three-illuminations/labels --output_dir train/split`
* Generiere Patches (Bildausschnitte) für das Training. Sowohl für den Trainings-Split als auch für den Validation-Split:
  * mkdir -p train/patches && python -m stain_detection.dataset.patch_stain_based --labels_dir train/three-illuminations/labels --laundry_ids train/split/ids_training.txt --output train/patches/training.csv 
  * mkdir -p train/patches && python -m stain_detection.dataset.patch_stain_based --labels_dir train/three-illuminations/labels --laundry_ids train/split/ids_validation.txt --output train/patches/validation.csv 
* Training:
  * Vor dem Training muss noch die Datei der vor-trainierten [Gewichte](https://uni-bielefeld.sciebo.de/s/XFzxq5DIqylTUJE) des Modells herunter geladen werden. Z.B.: CPD.pth
  * Erstellen eines Ordners zum speichern von Gewichten/Snapshots: `mkdir -p train/snapshots`
  * Durchführung des Trainings: `python -m stain_detection.train --model cpd --weights CPD.pth --dataset_dir train/three-illuminations --csv_dir train/patches --epochs 50 --batch_size 16 --val_epoch 10 --snapshot_epoch 10 --snapshot_dir train/snapshots --logfile train/train.log`
  * Noch einige andere Parameter für das Training sind einstellbar. Für alle Optionen inklusive Beschreibung einfach `python -m stain_detection.train -h` aufrufen.
  * Durch die Variante oben wird für 50 Epochen trainiert, wobei alle 10 Epochen der Loss auf dem Validationsteil berechnet wird und alle 10 Epochen die Gewichte zwischen gespeichert werden.
  * Nach Abschluss des Scripts liegen deshalb die Dateien `10.pth`, `20.pth`, `30.pth`, `40.pth` und `50.pth` im Ordner `train/snapshots` 
* Berechnungen der Salienzkarten für den Testteil: `mkdir -p train/saliency_maps && python -m stain_detection.compute_sams --model cpd --weights train/snapshots/50.pth --images_dir train/three-illuminations/images --output_dir train/saliency_maps --laundry_ids train/split/ids_test.txt`
* Evaluation:
  * Berechnung verschiedener Metriken: `python -m stain_detection.measures.compute_stats --predictions_dir train/saliency_maps --labels_dir train/three-illuminations/labels --output train/measures.csv --iterate --connect_regions`
  * Anzeigen der Region Detection Rate (RDR): `python -m stain_detection.vis.rdr_curve --csv_files train/measures.csv --labels Beispieltraining --false_prediction_weight 0.5`
  * Den Argumenten `--csv_files` und `--labels` können mehrere Werte übergeben werden, um die Werte aus verschiedenen Trainings zu Vergleichen.

## Aufbau
Das Projekt ist wie folgt strukturiert:
Unmittelbar im Ordner `stain_detection` befinden sich ein paar Scripte für generelle Funktionen.
Mit `stain_detection/train.py` kann ein Modell (weiter-)trainiert werden, mit `stain_detection/compute_sams.py` können Salienzkarten berechnet werden und mit `stain_detection/combine_sams.py` können mehrere Salienzkarten zu einer reduziert werden.
Des weiteren ist das Projekt in mehrere Submodule unterteilt, welche alle durch eigene Ordner in `stain_detection/` definiert sind:
* `dataset` enthält Code um die Daten als Datensätze für das Training vorzubereiten und zu laden.
* `measures` enthält Code um die Ergebnisse über verschiedene Metriken zu evaluieren.
* `models` enthält Code zum Nutzen verschiedener neuronaler Netze der Salienzerkennung.
* `util` enthält Utility Script die ich für verschiedene Untersuchungen genutzt habe.
* `vis` enthält Scripte für die Visualisierung.

### Dataset
* `split.py` erlaubt es die Splits der Daten anhand des Wäschestücks zu erstellen. Das wird fürs Training benötigt.
* `transforms.py` enthält Transformationen für die Datenaugmentation beim Training.
* `csv.py` enthält Code um einen Datensatz für das Training zu verwenden. Dafür wird der Ordner mit Daten benötigt sowie eine CSV-Datei in der Bildausschnitte definiert sind. So eine CSV-Datei kann mit den Scripten `patch_stain_based.py` und `patch_sliding_window.py` erstellt werden.

Der Unterschied zwischen den beiden Methoden für die Erstellung von Bildausschnitten ist, dass `patch_stain_based` Ausschnitte verschiedener Größe um gelabelte Flecken erstellt, während `patch_sliding_window` Ausschnitte in immer der gleiche Größe erstellt indem ein Fenster über das Ausgangsbild geschoben wird.

### Measures
* `pixel_wise.py`: Berechnung von pixelweise berechneten Metriken, wie Precision, Recall und Intersection over Union (IoU).
* `region_wise.py`: Berechnung von regionenweisen Metriken bzw. der Region Detection Rate (RDR).
* `compute_stats.py`: Berechnung der Daten auf denen die Metriken beruhen pro Bild (z.B. erkannte Regionen und falsche Vorhersagen). Das erlaubt verschiedene Parameter der Metriken zu nutzen.
* `from_stats.py`: Berechnung und Ausgabe der Metriken basierend auf einer mit `compute_stats.py` erstellten CSV-Datei.
* `util.py`: Utility Funktionen für die Berechnungen.

### Models
* `__init__.py` enthält Code zum laden der verschiedenen Modelle
* Die anderen Daten enthalten für die verschiedenen getesteten Modelle den Code, sowie Schritte zum Vorverarbeiten von Bildern und Nachbearbeiten der Outputs.
* Quellen zu den einzelnen Architekturen:
  * [BASNet](https://openaccess.thecvf.com/content_CVPR_2019/html/Qin_BASNet_Boundary-Aware_Salient_Object_Detection_CVPR_2019_paper.html)
  * [CPD](https://openaccess.thecvf.com/content_CVPR_2019/html/Wu_Cascaded_Partial_Decoder_for_Fast_and_Accurate_Salient_Object_Detection_CVPR_2019_paper.html)
  * [LDF](https://openaccess.thecvf.com/content_CVPR_2020/html/Wei_Label_Decoupling_Framework_for_Salient_Object_Detection_CVPR_2020_paper.html)
  * [PoolNet](https://openaccess.thecvf.com/content_CVPR_2019/html/Liu_A_Simple_Pooling-Based_Design_for_Real-Time_Salient_Object_Detection_CVPR_2019_paper.html)
  * [U2-Net](https://www.sciencedirect.com/science/article/abs/pii/S0031320320302077)

### Vis
* `loss.py`: Visualisieren den Wert der Lossfunktion die während des Trainings optimiert wird. Dafür wird der Log eines Trainings benötigt.
* `rdr_curve.py`: Visualisiere die RDR über verschiedene Schwellwerte.
* `side_by_side.py`: Visualisiere Bilder indem sie nebeneinander dargestellt werden.

## Daten
* Basisgewichte und vor-trainierte Gewichte verschiedener neuronaler Netze: [Weights](https://uni-bielefeld.sciebo.de/s/XFzxq5DIqylTUJE)
* Erster aufgenommener Datensatz mit einer Beleuchtung und blindem Fleck in der Mitte: [Dataset: Blindspot](https://uni-bielefeld.sciebo.de/s/53eIRuLhx4AjfwI)
* Zweiter aufgenommener Datensatz mit drei Beleuchtungen: [Dataset: Three-Illuminations](https://uni-bielefeld.sciebo.de/s/bEYoAuMiSQfywLa)
