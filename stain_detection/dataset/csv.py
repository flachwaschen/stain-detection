import os
import random

import cv2 as cv
import numpy as np
import pandas as pd
from PIL import Image
from torch.utils.data import Dataset


def replace_illumination(filename, illumination_id):
    dirname = os.path.dirname(filename)
    basename = os.path.basename(filename)
    basename, ext = os.path.splitext(basename)
    split = basename.split("-")
    if len(split) > 4:
        if illumination_id is None:
            split = split[:4]
        else:
            split[4] = str(illumination_id)
    else:
        if illumination_id is not None:
            split.append(str(illumination_id))
    basename = "-".join(split) + ext
    return os.path.join(dirname, basename)


class CSVDataset(Dataset):
    def __init__(
        self,
        csv_file,
        dir_dataset,
        model=None,
        augments=[],
        illuminations=None,
        random_illumination=True,
    ):
        self.table = pd.read_csv(csv_file)
        self.dir_dataset = dir_dataset
        self.model = model
        self.augments = augments
        self.illuminations = illuminations
        self.random_illumination = random_illumination

        if not self.random_illumination:

            def replace_illumination_in_row(row, illumination_id):
                row["filename"] = replace_illumination(row["filename"], illumination_id)
                return row

            tables = [
                self.table.apply(
                    lambda row: replace_illumination_in_row(row, _id), axis="columns"
                )
                for _id in self.illuminations
            ]
            self.table = pd.concat(tables)

    def __getitem__(self, index):
        row = self.table.iloc[index]
        filename = row["filename"]

        if self.illuminations is not None and self.random_illumination:
            random_index = random.randrange(len(self.illuminations))
            filename = replace_illumination(filename, self.illuminations[random_index])

        t, l = row["top"], row["left"]
        h, w = row["height"], row["width"]
        box = (l, t, l + w, t + h)

        image = Image.open(os.path.join(self.dir_dataset, "images", filename))
        image = image.crop(box).convert("RGB")

        label = Image.open(os.path.join(self.dir_dataset, "labels", filename))
        label = label.crop(box).convert("L")

        for augment in self.augments:
            image, label = augment(image, label)

        image = np.array(image)
        image = cv.cvtColor(image, cv.COLOR_RGB2BGR)
        if self.model is not None:
            image = self.model.preprocess_image(image)

        label = np.array(label)
        if self.model is not None:
            label = self.model.preprocess_label(label)

        return image, label

    def __len__(self):
        return self.table.shape[0]


class IlluminationChannelDataset:
    """
    Dataset which loads patches like a CSV Dataset, but loads image
    intensity of each illumination into a separate channel. E.g., the
    illumination below becomes the blue channel, the illumination above-
    front becomes the green channel and the illumination above-back
    the red channel.
    """

    @staticmethod
    def get_default_channel_mapping():
        return {"b": 0, "g": 1, "r": 2}

    @staticmethod
    def load_as_channel_img(filename, channel_mapping, box=None):
        channel_imgs = map(
            lambda channel: (
                channel,
                cv.imread(
                    replace_illumination(filename, channel_mapping[channel]),
                    cv.IMREAD_GRAYSCALE,
                ),
            ),
            ["r", "g", "b"],
        )
        if box is not None:
            channel_imgs = map(
                lambda channel_img_t: (
                    channel_img_t[0],
                    channel_img_t[1][box[1] : box[3], box[0] : box[2]],
                ),
                channel_imgs,
            )
        channel_imgs = dict(channel_imgs)

        image = np.empty((*channel_imgs["r"].shape, 3), dtype=np.uint8)
        for i, channel in enumerate(["r", "g", "b"]):
            image[:, :, i] = channel_imgs[channel]
        return image

    def __init__(
        self, csv_file, dir_dataset, model=None, augments=[], channel_mapping=None
    ):
        self.table = pd.read_csv(csv_file)
        self.dir_dataset = dir_dataset
        self.model = model
        self.augments = augments
        self.channel_mapping = (
            self.get_default_channel_mapping()
            if channel_mapping is None
            else channel_mapping
        )

    def __getitem__(self, index):
        row = self.table.iloc[index]
        filename = row["filename"]

        t, l = int(row["top"]), int(row["left"])
        h, w = int(row["height"]), int(row["width"])
        box = (l, t, l + w, t + h)

        image = self.load_as_channel_img(
            os.path.join(self.dir_dataset, "images", filename),
            self.channel_mapping,
            box,
        )
        image = Image.fromarray(image)

        _filename = replace_illumination(filename, None)
        label = Image.open(os.path.join(self.dir_dataset, "labels_combined", _filename))
        label = label.crop(box).convert("L")

        for augment in self.augments:
            image, label = augment(image, label)

        image = np.array(image)
        image = cv.cvtColor(image, cv.COLOR_RGB2BGR)
        if self.model is not None:
            image = self.model.preprocess_image(image)

        label = np.array(label)
        if self.model is not None:
            label = self.model.preprocess_label(label)

        return image, label

    def __len__(self):
        return self.table.shape[0]

