import argparse
import datetime
import os
import random

import cv2 as cv
import numpy as np
import pandas as pd
from tqdm import tqdm

from stain_detection.dataset.split import (
    get_laundry_id,
    get_illumination_id,
    read_ids,
    get_side,
)
from stain_detection.measures.util import compute_connected_components, BoundingBox
import stain_detection.util.patterns as patterns_util

parser = argparse.ArgumentParser(
    "Create a patch dataset with varying patch sizes",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "--labels_dir", type=str, required=True, help="directory of label files"
)
parser.add_argument(
    "--laundry_ids",
    type=str,
    help="optional file of laundry ids used to filter the files",
)
parser.add_argument(
    "--illumination_id",
    type=int,
    help="only use labels for a specific illumination id",
)
parser.add_argument(
    "--patch_size_min", type=int, default=352, help="the size of extracted patches"
)
parser.add_argument(
    "--patch_size_max", type=int, default=352 * 5, help="the size of extracted patches"
)
parser.add_argument(
    "--output",
    type=str,
    default="data.csv",
    help="the csv file written by this script",
)
parser.add_argument(
    "--patches_per_stain",
    type=int,
    default=2,
    help="the minimum amount of patches on which each stain is visible",
)
parser.add_argument(
    "--min_downscaled_size",
    type=int,
    default=5,
    help="for small stains to be visible, patches have to be small because they are further downscaled for training, this value ensures that stains will be at least 5 pixels in size after downscaling",
)
parser.add_argument(
    "--percentage_unlabeled",
    type=float,
    default=0.4,
    help="the percentage of patches containing no labeled pixel",
)
parser.add_argument(
    "--random_seed",
    type=int,
    help="seed the random number generator",
)
patterns_util.add_args(parser)
args = parser.parse_args()

if not args.random_seed:
    args.random_seed = int(datetime.datetime.now().timestamp())
print(f"RNG seed: {args.random_seed}")
random.seed(args.random_seed)

files = os.listdir(args.labels_dir)
if args.laundry_ids:
    laundry_ids = read_ids(args.laundry_ids)
    files = filter(lambda f: get_laundry_id(f) in laundry_ids, files)
if args.illumination_id:
    files = filter(lambda f: get_illumination_id(f) == args.illumination_id, files)
files = list(files)
files.sort()
files = patterns_util.filter_files(files, args)

total_stain_count = files
total_stain_count = map(
    lambda x: cv.imread(os.path.join(args.labels_dir, x), cv.IMREAD_GRAYSCALE),
    total_stain_count,
)
total_stain_count = map(compute_connected_components, total_stain_count)
total_stain_count = map(len, total_stain_count)
total_stain_count = tqdm(total_stain_count, total=len(files))
total_stain_count = sum(total_stain_count)

total_patch_count_with = total_stain_count * args.patches_per_stain
total_patch_count_without = (
    args.percentage_unlabeled
    * total_patch_count_with
    / (1.0 - args.percentage_unlabeled)
)
average_with = total_patch_count_without / len(files)
average_with = round(average_with)

data = pd.DataFrame(
    {"filename": [], "top": [], "left": [], "width": [], "height": [], "count": []}
)


def generate_bb_with(args, _max_size, cc, label):
    _size = random.randint(args.patch_size_min, _max_size)
    bb = cc.bounding_box

    if bb.width > _size:
        min_left = bb.left
        max_left = bb.right - _size
    else:
        min_left = max(bb.right - _size, 0)
        max_left = min(bb.left, label.shape[1] - _size)

    if bb.height > _size:
        min_top = bb.top
        max_top = bb.bottom - _size
    else:
        min_top = max(bb.bottom - _size, 0)
        max_top = min(bb.top, label.shape[0] - _size)
    _left = random.randint(min_left, max_left)
    _top = random.randint(min_top, max_top)
    return BoundingBox(_left, _top, _size, _size)


counts = []
for filename in files:
    print(f"Process {filename} ...", end="\r")
    label = cv.imread(os.path.join(args.labels_dir, filename), cv.IMREAD_GRAYSCALE)

    ccs = compute_connected_components(label)
    bbs = list(map(lambda cc: cc.bounding_box, ccs))

    print(f"Process {filename} stains ...", end="\r")
    for cc in ccs:
        s = min(cc.bounding_box.width, cc.bounding_box.height)
        _max_size = min(
            (s * args.patch_size_min) / args.min_downscaled_size, args.patch_size_max
        )
        _max_size = max(int(_max_size), args.patch_size_min)

        for i in range(args.patches_per_stain):  # Count of patches per cc
            _bb = generate_bb_with(args, _max_size, cc, label)
            data = data.append(
                {
                    "filename": filename,
                    "top": _bb.top,
                    "left": _bb.left,
                    "width": _bb.width,
                    "height": _bb.height,
                    "count": cc.area,
                },
                ignore_index=True,
            )

    print(f"Process {filename} empty ...", end="\r")
    tries = 0
    i = 0
    while i < average_with:
        tries += 1
        if tries > 100000:
            print(
                f"Skip trying to find empty patches on image {filename} after {tries} tries..."
            )
            break
        _size = random.randint(args.patch_size_min, args.patch_size_max)
        left = random.randint(0, label.shape[1] - _size)
        top = random.randint(0, label.shape[0] - _size)

        _bb = BoundingBox(left, top, _size, _size)
        _intersects = any(map(lambda __bb: __bb.intersects(_bb), bbs))
        if _intersects:
            continue

        tries = 0
        i += 1
        data = data.append(
            {
                "filename": filename,
                "top": _bb.top,
                "left": _bb.left,
                "width": _size,
                "height": _size,
                "count": 0,
            },
            ignore_index=True,
        )

    print(" " * 150, end="\r")

data.to_csv(args.output, index=False)
