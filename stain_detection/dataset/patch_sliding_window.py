import argparse
import csv
import os
import random

import cv2 as cv
import numpy as np
from tqdm import tqdm

from stain_detection.dataset.split import get_laundry_id, read_ids


class BoundingBox:
    def __init__(self, top, left, height, width):
        self.top = top
        self.bottom = top + height
        self.left = left
        self.right = left + width
        self.width = width
        self.height = height

    def as_slice(self):
        return (slice(self.top, self.bottom), slice(self.left, self.right))


class Patches:
    def __init__(self, img, size, stride):
        self.img = img
        self.size = size
        self.stride = stride

        self.ys = list(range(0, img.shape[0] - size, stride))
        self.xs = list(range(0, img.shape[1] - size, stride))
        if len(self.ys) == 0 or len(self.xs) == 0:
            raise ValueError(
                f"Patch size {size} is larger then the image {img.shape[:2]}"
            )

        if self.ys[-1] != img.shape[0] - size:
            self.ys.append(img.shape[0] - size)
        if self.xs[-1] != img.shape[1] - size:
            self.xs.append(img.shape[1] - size)

        self.len_x = len(self.xs)
        self.len_y = len(self.ys)

    def __len__(self):
        return self.len_x * self.len_y

    def __get__(self, i):
        iy = i // self.len_x
        ix = i - (self.len_x * iy)

        top = self.ys[iy]
        left = self.xs[ix]
        bb = BoundingBox(top, left, self.size, self.size)

        return self.img[bb.as_slice()], (top, left)

    def __iter__(self):
        self.i = 0
        return self

    def __next__(self):
        if self.i >= len(self):
            raise StopIteration

        _next = self.__get__(self.i)
        self.i += 1
        return _next


def main(args):
    files = os.listdir(args.labels_dir)

    if args.laundry_ids:
        ids = read_ids(args.laundry_ids)
        files = list(filter(lambda filename: get_laundry_id(filename) in ids, files))

    data = []
    for filename in tqdm(files):
        label = cv.imread(os.path.join(args.labels_dir, filename), cv.IMREAD_GRAYSCALE)
        for patch, (top, left) in Patches(label, args.patch_size, args.patch_stride):
            count = (patch == 255).sum()
            data.append(
                {
                    "filename": filename,
                    "top": top,
                    "left": left,
                    "width": patch.shape[1],
                    "height": patch.shape[0],
                    "count": count,
                }
            )
    with_labels = list(map(lambda row: row["count"], data))
    with_labels = np.array(with_labels) > 0
    with_labels = with_labels.sum()
    print(f"Available Patches {len(data)} with labeled pixels {with_labels}")

    from_labeled = int(with_labels * args.percentage_from_labeled)
    from_unlabeled = int(
        from_labeled / (1.0 - args.percentage_unlabeled) - from_labeled
    )
    from_unlabeled = min(len(data) - from_labeled, from_unlabeled)
    total = from_labeled + from_unlabeled

    print(
        f"Labeled : Unlabeled = {from_labeled} : {from_unlabeled} = {from_labeled / total:.2f} : {from_unlabeled / total:.2f}"
    )

    count_labeled = 0
    count_unlabeled = 0
    _data = []
    random.shuffle(data)
    for row in data:
        if row["count"] > 0 and count_labeled < from_labeled:
            _data.append(row)
            count_labeled += 1

        if row["count"] == 0 and count_unlabeled < from_unlabeled:
            _data.append(row)
            count_unlabeled += 1

        if count_labeled == from_labeled and count_unlabeled == from_unlabeled:
            break

    with open(args.output, mode="w") as f:
        writer = csv.DictWriter(f, fieldnames=data[0].keys())
        writer.writeheader()
        writer.writerows(_data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Create a dataset of patches with the same size",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--laundry_ids",
        type=str,
        help="optional file of laundry ids used to filter the files",
    )
    parser.add_argument(
        "--labels_dir", type=str, required=True, help="directory of label files"
    )
    parser.add_argument(
        "--patch_size", type=int, default=512, help="the size of extracted patches"
    )
    parser.add_argument(
        "--patch_stride", type=int, default=256, help="the stride for extracted patches"
    )
    parser.add_argument(
        "--output",
        type=str,
        default="data.csv",
        help="the csv file written by this script",
    )
    parser.add_argument(
        "--percentage_from_labeled",
        type=float,
        default=1.0,
        help="the percentage of patches containing at least a single labeled pixel taken into the dataset",
    )
    parser.add_argument(
        "--percentage_unlabeled",
        type=float,
        default=0.5,
        help="the percentage of patches containing no labeled pixel",
    )
    args = parser.parse_args()
    main(args)
