"""
Split a dataset of laundry into training-, validation- and testset.
This is done through the laundry id parsed from the filename to ensure
that different splits do not contain the same piece of laundry.
The result are three files containing the id of a piece of laundry
in each line.
"""

import argparse
import datetime
import os
import random

import cv2 as cv
import numpy as np
import pandas as pd
import tqdm

import stain_detection.util.patterns as patterns_util


def _get_basename(filename):
    basename = os.path.basename(filename)
    basename, _ = os.path.splitext(basename)
    return basename

def get_laundry_id(filename):
    basename = _get_basename(filename)
    return int(basename.split("-")[3])


def get_illumination_id(filename):
    basename = _get_basename(filename)
    return int(basename.split("-")[4])


def get_side(filename):
    basename = _get_basename(filename)
    return basename.split("-")[1]


def parse_ratio(str_ratio):
    _split = str_ratio.split(":")
    assert len(_split) == 3

    ratios = list(map(int, _split))
    ratios = np.array(ratios)
    assert np.sum(ratios) == 100
    return ratios / 100


def read_ids(filename):
    ids = []
    with open(filename, mode="r") as f:
        for line in f:
            ids.append(int(line.strip()))
    return ids


def write_ids(ids, filename):
    with open(filename, mode="w") as f:
        for _id in ids:
            f.write(f"{_id}\n")


def read_labels(label_files):
    # read label files and accumulate labeled pixel count by laundry id
    counts_by_laundry_id = {}
    for label_file in tqdm.tqdm(label_files):
        laundry_id = get_laundry_id(label_file)

        label = cv.imread(label_file, cv.IMREAD_GRAYSCALE)
        count = (label == 255).sum()
        counts_by_laundry_id[laundry_id] = (
            counts_by_laundry_id.get(laundry_id, 0) + count
        )

    # unpack into two lists of ids and counts
    counts = []
    ids = []
    for key, val in counts_by_laundry_id.items():
        ids.append(key)
        counts.append(val)
    return np.array(ids), np.array(counts)


def creats_split(ids, ratios):
    indices = list(range(len(ids)))
    random.shuffle(indices)

    x = round(len(ids) * ratios[0])
    y = round(len(ids) * (ratios[0] + ratios[1]))
    return indices[:x], indices[x:y], indices[y:]


def validation_criteria(values):
    """
    Validate that each value differs at most 0.1 from the
    average of all values
    """
    avg = np.average(values)
    valid = np.abs(values - avg) < 0.1 * avg
    return valid.all()


def validate_split(idxs, counts):
    """
    Validate the splits created through the list of list in idxs
    by making sure that the average, median and std of these
    splits is similar.
    """
    splits_counts = list(map(lambda _idx: counts[_idx], idxs))

    functions = [np.average, np.median, np.std]
    for f in functions:
        values = list(map(f, splits_counts))
        values = np.array(values)
        if not validation_criteria(values):
            return False
    return True


def main(args):
    files = [os.path.join(args.labels_dir, f) for f in os.listdir(args.labels_dir)]
    files.sort()
    files = patterns_util.filter_files(files, args)

    ids, counts = read_labels(files)
    ratios = parse_ratio(args.split)

    idxs = creats_split(ids, ratios)
    if not args.skip_validation:
        while not validate_split(idxs, counts):
            idxs = creats_split(ids, ratios)

    names = ["training", "validation", "test"]
    for name, _idxs in zip(names, idxs):
        _ids = ids[_idxs]
        filename = os.path.join(args.output_dir, f"ids_{name}.txt")
        write_ids(_ids, filename)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="split a dataset of laundry images into training-, validation and testset through their laundry id",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--labels_dir",
        type=str,
        required=True,
        help="directory containing the label files",
    )
    parser.add_argument(
        "-s",
        "--split",
        type=str,
        default="70:15:15",
        help="ratios of files for the different sets training:validation:test",
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        default="./",
        help="the directory to which the id files are written",
    )
    parser.add_argument(
        "--random_seed",
        type=int,
        help="seed the random number generator",
    )
    parser.add_argument(
        "--skip_validation",
        action="store_true",
        help="skip the validation of the split",
    )
    patterns_util.add_args(parser)
    args = parser.parse_args()

    if not args.random_seed:
        args.random_seed = int(datetime.datetime.now().timestamp())
    print(f"RNG seed: {args.random_seed}")
    random.seed(args.random_seed)

    main(args)
