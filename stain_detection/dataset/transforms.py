import random
import torchvision.transforms.functional as TF


class RandomHorizontalFlip:
    def __init__(self, flip_probability=0.5):
        self.flip_probability = flip_probability

    def __call__(self, image, label):
        if random.random() > self.flip_probability:
            image = TF.hflip(image)
            label = TF.hflip(label)
        return image, label


class RandomVerticalFlip:
    def __init__(self, flip_probability=0.5):
        self.flip_probability = flip_probability

    def __call__(self, image, label):
        if random.random() > self.flip_probability:
            image = TF.vflip(image)
            label = TF.vflip(label)
        return image, label
