import argparse
import os

import numpy as np
import matplotlib.pyplot as plt


SIZE_DEFAULT = 12
plt.rc("font", family="Roboto")  # controls default font
plt.rc("font", weight="normal")  # controls default font
plt.rc("font", size=SIZE_DEFAULT)  # controls default text sizes
plt.rc("axes", titlesize=18)  # fontsize of the axes title

# https://colorbrewer2.org/#type=diverging&scheme=RdBu&n=3lk
COLORS = ["#ef8a62", "#67a9cf"]


def parse_losses(log_file, initial_loss=False):
    losses = {"VAL": [], "TRAIN": []}
    with open(log_file, mode="r") as f:
        for line in f:
            if "INFO" in line:
                line = line.split("INFO: ")[1]
            s = line.strip().split()
            if len(s) == 2:
                continue

            phase = s[0].replace(":", "")

            if phase != "TRAIN" and phase != "VAL":
                continue

            epoch = s[2]
            epoch = int(epoch.split("/")[0])

            if epoch == 0 and not initial_loss:
                continue

            loss = float(s[4])

            losses[phase].append((epoch, loss))
    return losses


def main(args):
    if args.labels:
        if len(args.labels) != len(args.log_files):
            print(
                f"Label count {len(args.labels)} does not match log file count {len(args.log_files)}"
            )

        labels = args.labels
    else:
        labels = [filename.split("/")[-1].split(".")[0] for filename in args.log_files]
    loss_train = []
    loss_validation = []

    epoch_train = []
    epoch_validation = []

    loss_map = {}
    max_loss = 0
    for log_file in args.log_files:
        name = os.path.splitext(os.path.basename(log_file))[0]
        losses = parse_losses(log_file, initial_loss=args.initial_loss)

        loss_map[name] = {}

        for loss_type in losses:
            loss_map[name][loss_type] = np.array(losses[loss_type])

            max_loss = max(max_loss, np.max(loss_map[name][loss_type][:, 1]))

    fig, ax = plt.subplots(figsize=(9, 6))
    for i, name in enumerate(loss_map.keys()):
        losses = loss_map[name]
        for loss_type in losses:
            loss = losses[loss_type][:, 1]
            epochs = losses[loss_type][:, 0]

            min_loss = np.min(loss)
            min_epoch = epochs[np.argmin(loss)]

            print(
                f"{name} min-{loss_type.lower()}-loss {min_loss:.3f} at iteration {min_epoch}"
            )

            if args.normalize:
                loss = loss / max_loss

            if loss_type == "VAL":
                label = "Validation"
            elif loss_type == "TRAIN":
                label = "Train"
            else:
                label = name + "_" + loss_type

            if len(labels) > 1:
                label = f"{labels[i]} - {label}"

            color = COLORS[0] if loss_type == "VAL" else COLORS[1]
            ax.plot(epochs, loss, label=label, color=color)
            ax.scatter(epochs, loss, s=15, color=color)

    if args.normalize:
        ax.set_ylim(0.0, 1.05)
    if args.ylim:
        ax.set_ylim(args.ylim)

    ax.spines["left"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)

    ax.grid(axis="y", alpha=0.7)
    ax.legend()
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Loss")
    if args.title:
        ax.set_title(args.title)
    plt.tight_layout()

    if args.output:
        plt.savefig(args.output, dpi=300)
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Plot the losses from a training log file",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "log_files",
        type=str,
        nargs="+",
        help='the log files to be processed in the form "<label>.log"',
    )
    parser.add_argument(
        "--normalize",
        action="store_true",
        help="normalize losses to [0, 1] by dividing all values through the highest",
    )
    parser.add_argument(
        "--initial_loss", action="store_true", help="include the loss at iteration 0"
    )
    parser.add_argument(
        "--labels",
        type=str,
        nargs="+",
        help="labels for the curves - is chosen based on the filename by default",
    )
    parser.add_argument("--title", type=str, help="optional title for the plot")
    parser.add_argument("--output", type=str, help="save the figure as this file")
    parser.add_argument(
        "--ylim", type=float, nargs=2, help="manually provide limits for the y axis"
    )
    args = parser.parse_args()

    main(args)
