import argparse
import os

import cv2 as cv
import numpy as np

def main(args):
    cv.namedWindow(args.window_title, cv.WINDOW_NORMAL)
    cv.resizeWindow(args.window_title, 1920, 1080)

    image_files = os.listdir(args.directories[0])
    image_files.sort()

    for image_file in image_files:
        print(f"Show: {image_file}")
        images = [cv.imread(os.path.join(d, image_file), cv.IMREAD_COLOR) for d in args.directories]
        for i in range(1, len(images)):
            images[i] = np.full(images[0].shape, 200) if images[i] is None else images[i]

        height_combined = images[0].shape[0]
        width_combined = sum(map(lambda img: img.shape[1], images)) + args.offset * len(images)
        channels_combined = images[0].shape[2]
        shape_combined = (height_combined, width_combined, channels_combined)
        combined = np.ones(shape_combined, dtype=np.uint8) * 127

        for i, image in enumerate(images):
            w_fr = i * image.shape[1] + i * args.offset
            w_to = (i + 1) * image.shape[1] + i * args.offset
            combined[:, w_fr:w_to] = image

        cv.imshow(args.window_title, combined)
        key = cv.waitKey(0)
        if key == ord("q"):
            break
        elif key == ord("s"):
            cv.imwrite(os.path.join(args.output_dir, image_file), combined)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="show images from different directories side by side",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("directories", type=str, nargs="+",
                        help="the directories from which images are loaded")
    parser.add_argument("--window_title", type=str, default="Side by Side",
                        help="the displayed window title")
    parser.add_argument("--filter", type=str,
                        help="an optional mapping file to only show laundry with ids in the mapping")
    parser.add_argument("--offset", type=int, default=10,
                        help="offset between images")
    parser.add_argument("--output_dir", type=str, default="./",
                        help="directory where combined images are saved to")
    args = parser.parse_args()

    main(args)

