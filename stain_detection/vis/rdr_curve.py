import argparse
import csv
import math
import os

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import termcolor

import stain_detection.measures.region_wise as region_wise


SIZE_DEFAULT = 12
plt.rc("font", family="Roboto")  # controls default font
plt.rc("font", weight="normal")  # controls default font
plt.rc("font", size=SIZE_DEFAULT)  # controls default text sizes
plt.rc("axes", titlesize=18)  # fontsize of the axes title

COLORS = [
    "#1b9e77",
    "#d95f02",
    "#7570b3",
    "#e7298a",
    "#66a61e",
]


def area_under_curve(xs, ys):
    area = 0
    for x_0, x_1, y_0, y_1 in zip(xs[:-1], xs[1:], ys[:-1], ys[1:]):
        delta_x = x_1 - x_0
        delta_y = abs(y_1 - y_0)

        min_y = min(y_1, y_0)

        area += delta_x * min_y
        area += delta_x * delta_y / 2
    return area


def auc(xs, ys):
    return area_under_curve(xs, ys)


def compute_rdr_curve(stats, fpr_weight):
    xs = stats["threshold"].unique()
    ys = map(lambda x: stats[stats["threshold"] == x].sum(), xs)
    ys = map(region_wise.RegionQuantities.from_dict, ys)
    ys = map(lambda y: region_wise.rdr(y, false_prediction_weight=fpr_weight), ys)
    ys = list(ys)
    return xs, ys


def compute_scores(curve, stats):
    xs, ys = curve

    xs_normalized = (xs - xs.min()) / (xs.max() - xs.min())
    auc = area_under_curve(xs_normalized, ys)

    idx = np.argmax(ys)
    rdr = ys[idx]
    threshold = xs[idx]

    score = auc + rdr

    qs = stats[stats["threshold"] == threshold].sum()
    qs = region_wise.RegionQuantities.from_dict(qs)

    return {
        "AUC": auc,
        "RDR": rdr,
        "Score": score,
        "Theshold": threshold,
        "Detected-Regions": qs.detected_regions,
        "False-Predictions": qs.false_predictions,
        "Regions": qs.regions,
    }


def print_table(data, highlights=None):
    keys = list(data.keys())

    lenghtes = map(len, keys)
    lenghtes = map(lambda l: max(8, l + 4), lenghtes)
    lenghtes = list(lenghtes)

    line = map(lambda z: f"{z[0]:>{z[1]}}", zip(keys, lenghtes))
    line = "".join(line)
    print(line)

    if highlights is None:
        highlights = keys
    highlights = dict([(k, np.max(data[k])) for k in highlights])

    for i in range(len(data[keys[0]])):
        line = [data[k][i] for k in keys]
        line_str = map(lambda v: f"{v:.3f}" if isinstance(v, float) else str(v), line)
        line_str = map(lambda x: f"{x[0]:>{x[1]}}", zip(line_str, lenghtes))
        line_str = map(
            lambda x: termcolor.colored(x[2], "green")
            if x[0] in highlights and x[1] == highlights[x[0]]
            else x[2],
            zip(keys, line, line_str),
        )
        line_str = "".join(line_str)
        print(line_str)


def main(args):
    if args.labels is not None:
        assert len(args.labels) == len(args.csv_files)
    else:
        args.labels = args.csv_files

    fig, ax = plt.subplots(figsize=(9, 6))
    ax.spines["left"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)

    false_prediction_weight = 0.5 if args.false_prediction_weight == None else args.false_prediction_weight
    data = []
    for i, (label, filename) in enumerate(zip(args.labels, args.csv_files)):
        stats = pd.read_csv(filename)

        curve = compute_rdr_curve(stats, fpr_weight=false_prediction_weight)
        xs, ys = curve

        color = COLORS[i % len(COLORS)]
        ax.plot(xs, ys, label=label, color=color)

        _data = {"Label": label}
        _data.update(compute_scores(curve, stats))
        data.append(_data)

    table = dict([(k, []) for k in data[0].keys()])
    for k in table.keys():
        for i in range(len(data)):
            table[k].append(data[i][k])
    print_table(table, highlights=["AUC", "RDR", "Score", "Detected-Regions"])

    plt.xlabel("Threshold")
    plt.ylabel("Region Detection Rate")

    if args.xlim:
        ax.set_xlim(args.xlim)
    else:
        ax.set_xlim([xs.min() - 1, xs.max() + 1])

    if args.ylim:
        ax.set_ylim(args.ylim)

    legend_title = args.legend_title if args.legend_title else None
    ax.legend(title=legend_title)

    ax.grid(axis="y", alpha=0.7)

    if args.title:
        ax.set_title(args.title)

    plt.tight_layout()
    if args.output:
        plt.savefig(args.output, dpi=300)
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "plot the RDR over the salience map threshold",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--csv_files",
        nargs="+",
        type=str,
        required=True,
        help="the csv files containing the evaluated measures",
    )
    parser.add_argument(
        "--labels", nargs="+", type=str, help="labels for each curve in the plot"
    )
    parser.add_argument("--false_prediction_weight", type=float, help="weight to compute the RDR")
    parser.add_argument("--title", type=str, help="optional title for the plot")
    parser.add_argument("--legend_title", type=str, help="optional title for the legend")
    parser.add_argument("--output", type=str, help="save the figure as this file")
    parser.add_argument(
        "--ylim",
        type=float,
        nargs=2,
        help="provide parameters for the y limits of the plot",
    )
    parser.add_argument(
        "--xlim",
        type=float,
        nargs=2,
        help="provide parameters for the x limits of the plot",
    )

    args = parser.parse_args()
    main(args)
