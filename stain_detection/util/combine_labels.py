"""
Combine labels form all illuminations.
These combined labels are needed to compare predictions computed for multiple predictions.
"""

import argparse
import os

import cv2 as cv
import numpy as np


from stain_detection.util.strip_illumination_id import strip_illum_id


ILLUMINATION_IDS = [0, 1, 2]

parser = argparse.ArgumentParser(
    description="Combine labels form all illuminations.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "--labels_dir", type=str, required=True, help="directory of the labels"
)
parser.add_argument(
    "--output_dir",
    type=str,
    required=True,
    help="the directory to which the combined labels are saved",
)
args = parser.parse_args()

files = os.listdir(args.labels_dir)
files = set(map(strip_illum_id, files))
files = list(files)
files.sort()

for basename in files:
    print(f"Process {basename}", end="\r")
    labels = map(
        lambda illum_id: os.path.join(args.labels_dir, f"{basename}-{illum_id}.png"),
        ILLUMINATION_IDS,
    )
    labels = list(labels)

    labels = map(lambda filename: cv.imread(filename, cv.IMREAD_GRAYSCALE), labels)
    labels = list(labels)

    combined = np.maximum(*labels)
    cv.imwrite(os.path.join(args.output_dir, f"{basename}.png"), combined)
