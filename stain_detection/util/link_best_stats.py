import argparse
import os

import pandas as pd

import stain_detection.measures.region_wise as region_wise

parser = argparse.ArgumentParser(
    description="link the best stats (depending on the rdr) file from multiple files",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "--inputs",
    type=str,
    nargs="+",
    required=True,
    help="directories and csv files which are parsed to find the best",
)
parser.add_argument(
    "--output",
    type=str,
    required=True,
    help="the output file to which the best input is linked",
)
parser.add_argument(
    "--false_prediction_weight",
    type=float,
    default=0.5,
    help="the false prediction weight for the rdr",
)
parser.add_argument(
    "--dry", action="store_true", help="do not create a link but just print the results"
)
parser.add_argument(
    "--ignore",
    type=str,
    nargs="*",
    help="names of files to ignore",
    default=[],
)
args = parser.parse_args()

inputs = []
for _input in args.inputs:
    if os.path.isdir(_input):
        inputs.extend([os.path.join(_input, f) for f in os.listdir(_input)])
    else:
        inputs.append(_input)

csvs = filter(lambda f: f.endswith(".csv"), inputs)
csvs = filter(lambda f: os.path.basename(f) not in args.ignore, csvs)
csvs = list(csvs)
csvs.sort()

best = {"rdr": -1, "filename": "", "threshold": -1}
for csv in csvs:
    print(f"Process {csv} ...", end="\r")
    stats = pd.read_csv(csv)

    for t in stats["threshold"].unique():
        _stats = stats[stats["threshold"] == t].sum()

        q = region_wise.RegionQuantities.from_dict(_stats)
        score = region_wise.rdr(q, false_prediction_weight=args.false_prediction_weight)

        if score > best["rdr"]:
            best["rdr"] = score
            best["filename"] = csv
            best["threshold"] = t
max_len = max(map(len, csvs))
print(" " * max_len, end="\r")

print(
    f"Best score {best['rdr']:.3f} at threshold {best['threshold']:03d} in file {best['filename']}"
)
if not args.dry:
    os.symlink(os.path.abspath(best["filename"]), args.output)
