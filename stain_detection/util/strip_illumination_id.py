import argparse
import os
import shutil


def strip_illum_id(filename):
    basename, ext = filename.split(".")
    _split = basename.split("-")

    if len(_split) != 5:
        raise ValueError(
            f"Filename {filename} does not contain five elements split by '-'. The last of which should be the illumination id."
        )

    _split = _split[:-1]
    return "-".join(_split)


parser = argparse.ArgumentParser(
    description="Strip the illumination id of files in a directory",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "--input_dir", type=str, required=True, help="directory of the labels"
)
parser.add_argument(
    "--output_dir",
    type=str,
    help="the directory to which the stripped files are saved, defaults to input_dir if not defined",
)
parser.add_argument("--force", action="store_true", help="skip user input to check if everything is fine")
args = parser.parse_args()

if args.output_dir is None:
    args.output_dir = args.input_dir

if not args.force:
    if args.input_dir == args.output_dir:
        print("ATTENTION: input dir == output dir will move files")
    print(
        "Make sure that the input dir does not contain multiple illuminations for the same file. Else, the behaviour which is taken is undefined!"
    )
    _input = input("Proceed (y|n): ")

    if str(_input).lower() != "y":
        exit(0)

files = os.listdir(args.input_dir)
files.sort()

for in_f in files:
    out_f = f"{strip_illum_id(in_f)}.png"

    in_f = os.path.join(args.input_dir, in_f)
    out_f = os.path.join(args.output_dir, out_f)

    if args.input_dir == args.output_dir:
        shutil.move(in_f, out_f)
    else:
        shutil.copyfile(in_f, out_f)
