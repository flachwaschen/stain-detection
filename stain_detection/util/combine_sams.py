import argparse
import os

import cv2 as cv
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("salience_maps_dirs", nargs="+", type=str, help="directories of sams to be combined")
parser.add_argument("-o", "--output_dir", type=str, required=True, help="the directory where combined salience maps are saved")
args = parser.parse_args()

def join(_dir, f):
    _files = os.listdir(_dir)
    illum_id = _files[0].split(".")[0].split("-")[-1]

    basename, ext = f.split(".")
    _s = basename.split("-")
    _s[-1] = illum_id + "." + ext
    _f = "-".join(_s)
    return os.path.join(_dir, _f)


files = os.listdir(args.salience_maps_dirs[0])
files.sort()
for f in files:
    print(f"Process {f}", end="\r")
    _files = [join(_dir, f) for _dir in args.salience_maps_dirs]
    sams = [cv.imread(_f, cv.IMREAD_GRAYSCALE) for _f in _files]
    sam = np.maximum(*sams) if len(sams) > 1 else sams[0]

    basename, ext = f.split(".")
    basename = basename.split("-")[:-1]
    basename = "-".join(basename)
    _f = f"{basename}.{ext}"

    cv.imwrite(os.path.join(args.output_dir, _f), sam)
