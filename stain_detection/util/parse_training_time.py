import argparse
from dateutil import parser as date_parser
from functools import reduce


def parse_time(logfile):
    with open(logfile, mode="r") as f:
        lines = f.readlines()
    start = date_parser.parse(lines[0].split("-")[0].strip())
    end = date_parser.parse(lines[-1].split("-")[0].strip())
    return end - start


parser = argparse.ArgumentParser()
parser.add_argument("logfiles", type=str, nargs="+")
args = parser.parse_args()

times = list(map(parse_time, args.logfiles))
times_sum = reduce(lambda a, b: a + b, times)

if len(args.logfiles) == 1:
    print(f"Training took {times[0]}")
    exit(0)

print("Training took:")
print(f"  - Average: {times_sum / len(args.logfiles)}")
print(f"  - Minimum: {min(times)}")
print(f"  - Maximum: {max(times)}")
print(f"  - Sum:     {times_sum}")
