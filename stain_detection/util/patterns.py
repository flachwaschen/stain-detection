"""
Utility to filter files by the patterns visible on laundry.
"""
import os
import pandas as pd


def _get_basename(filename):
    basename = os.path.basename(filename)
    basename, _ = os.path.splitext(basename)
    return basename

def get_laundry_id(filename):
    basename = _get_basename(filename)
    return int(basename.split("-")[3])


def get_side(filename):
    basename = _get_basename(filename)
    return basename.split("-")[1]



def add_args(parser):
    parser.add_argument(
        "--patterns_csv",
        type=str,
        help="csv file defining the pattern for each item of laundry",
    )
    parser.add_argument(
        "--exclude_patterns",
        type=int,
        nargs="*",
        help="pattern ids that should not appear in the dataset",
    )
    parser.add_argument(
        "--include_patterns",
        type=int,
        nargs="*",
        help="only these patterns should be in the dataset",
    )
    return parser


def filter_files(files, args):
    if not args.patterns_csv:
        return files

    patterns = pd.read_csv(args.patterns_csv)
    def has_pattern(filename, pattern_ids):
        laundry_id = get_laundry_id(filename)
        side = "front" if get_side(filename) == "V" else "back"
        pattern = patterns[
            (patterns["laundry_id"] == laundry_id) & (patterns["side"] == side)
        ]
        assert (
            len(pattern) == 1
        ), f"There should only be a single pattern definition for laundry {laundry_id} side {side}"
        return pattern["pattern_id"].iloc[0] in pattern_ids

    if args.include_patterns:
        files = filter(lambda f: has_pattern(f, args.include_patterns), files)
    if args.exclude_patterns:
        files = filter(lambda f: not has_pattern(f, args.exclude_patterns), files)
    return list(files)
