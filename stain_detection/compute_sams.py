import argparse
import enum
import os

import cv2 as cv
import numpy as np
from PIL import Image
import torch
import torchvision
from tqdm import tqdm


from stain_detection.dataset.csv import IlluminationChannelDataset
from stain_detection.dataset.split import get_laundry_id, get_illumination_id, read_ids
from stain_detection.dataset.patch_sliding_window import BoundingBox, Patches

from stain_detection.models import add_args, load_model, models

import stain_detection.util.patterns as patterns_util


class OverlapHandling(enum.Enum):
    average = "average"
    maximum = "maximum"

    def __str__(self):
        return self.value


parser = argparse.ArgumentParser(
    description="Compute the salience maps on alle images in a directory in a patch-wise fashion",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

# Model
add_args(parser)

# Dataset
parser.add_argument(
    "--images_dir",
    type=str,
    required=True,
    help="directory where to find the images to process",
)
parser.add_argument(
    "--laundry_ids", type=str, help="id file to filter images based on laundry ids"
)
parser.add_argument(
    "--illumination_id",
    type=int,
    default=1,
    help="id file to filter images by their illumination id",
)
parser.add_argument(
    "--output_dir",
    type=str,
    default="./",
    help="directory where to store the computed salience maps",
)
parser.add_argument(
    "--illuminations_to_channel",
    action="store_true",
    help="set the dataset to load all three illuminations into their own channel",
)
parser.add_argument(
    "--channel_mapping",
    default=["r", "g", "b"],
    nargs=3,
    help="if --illuminations_to_channel=True, define the channel with the three letter r, g, b. The below illumination is mapped to the first letter, the above-front to the second and above-back to the third.",
)

# Processing
parser.add_argument(
    "--patch_size",
    type=int,
    default=512,
    help="the size of patches extracted from input images",
)
parser.add_argument(
    "--stride",
    type=int,
    default=512,
    help="the stride with which patches are moved over input images",
)
parser.add_argument(  # TODO: include into model definition
    "--batch_size",
    type=int,
    default=8,
    help="patches are gathered into batches of this size for faster processing",
)
parser.add_argument(
    "--overlap_handling",
    type=OverlapHandling,
    default=OverlapHandling.average,
    choices=list(OverlapHandling),
    help="if the stride is smaller than the patch size there are overlapping regions - define how these are dealt with",
)
patterns_util.add_args(parser)
args = parser.parse_args()
args.channel_mapping = dict(zip(args.channel_mapping, [0, 1, 2]))

torch.set_grad_enabled(False) # gradients are not required for forward pass
device = torch.device(args.device)
model = load_model(args.model, args.weights, device)
model = model.eval()

files = os.listdir(args.images_dir)
if args.laundry_ids:
    ids = read_ids(args.laundry_ids)
    files = filter(lambda filename: get_laundry_id(filename) in ids, files)
if args.illumination_id in [0, 1, 2]:
    files = filter(
        lambda filename: get_illumination_id(filename) == args.illumination_id, files
    )
else:
    print(f"Ignore invalid illumination id {args.illumination_id}")
files = list(files)
files = patterns_util.filter_files(files, args)

for filename in tqdm(files):
    abs_filename = os.path.join(args.images_dir, filename)
    if args.illuminations_to_channel:
        img = IlluminationChannelDataset.load_as_channel_img(abs_filename, args.channel_mapping)
        # normally this conversion is part of the loading done in the dataset,
        # because here a utility method is used, the conversion is done by hand
        img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
    else:
        img = cv.imread(abs_filename, cv.IMREAD_COLOR)

    patches = Patches(img, args.patch_size, args.stride)
    patches = list(patches)

    sam = np.zeros(img.shape[:2])

    # count for each pixel how often it is set to compute an average
    # in case of OverlapHandling.average
    counts = np.zeros(sam.shape)

    _range = range(0, len(patches), args.batch_size)
    _range = list(_range)
    _range.append(len(patches))
    _range = list(zip(_range[:-1], _range[1:]))
    for i, j in tqdm(_range):
        batch = patches[i:j]

        coords = map(lambda b: b[1], batch)
        coords = map(
            lambda coord: BoundingBox(*coord, args.patch_size, args.patch_size), coords
        )
        coords = list(coords)

        batch_images = map(lambda b: b[0], batch)
        batch_images = map(model.preprocess_image, batch_images)
        batch_images = list(batch_images)
        inputs = torch.stack(batch_images)
        inputs = inputs.to(device)

        outputs = model(inputs)
        salience_maps = model.salience_map(outputs)
        salience_maps = salience_maps.cpu().detach().numpy()
        salience_maps = (salience_maps * 255).astype(np.uint8)

        for _sam, _coord in zip(salience_maps, coords):
            _sam = cv.resize(_sam, (args.patch_size, args.patch_size))

            if args.overlap_handling == OverlapHandling.average:
                counts[_coord.as_slice()] += 1
                sam[_coord.as_slice()] += _sam
            elif args.overlap_handling == OverlapHandling.maximum:
                sam[_coord.as_slice()] = np.maximum(_sam, sam[_coord.as_slice()])
            else:
                print(f"Overlap Handling {args.overlap_handling} not implemented!")
                exit(1)

    if args.overlap_handling == OverlapHandling.average:
        if (counts == 0).any():
            print(f"Could not average because for some region no values were computed!")
            exit(1)
        sam = sam / counts

    sam = sam.astype(np.uint8)
    cv.imwrite(os.path.join(args.output_dir, filename), sam)
