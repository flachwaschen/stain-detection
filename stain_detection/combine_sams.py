import argparse
import enum
from functools import reduce
import os

import cv2 as cv
import numpy as np
from tqdm import tqdm


class CombinationType(enum.Enum):
    average = "average"
    maximum = "maximum"
    weights = "weights"

    def __str__(self):
        return self.value


parser = argparse.ArgumentParser(
    description="Combine salience maps from different folder to for a final prediction",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "directories", type=str, nargs="+", help="the directories containing salience maps"
)
parser.add_argument(
    "--output_dir",
    type=str,
    default="./",
    help="directory where to store the combined salience maps",
)
parser.add_argument(
    "--combination",
    type=CombinationType,
    default=CombinationType.maximum,
    choices=list(CombinationType),
    help="define how the salience maps should be combined",
)
parser.add_argument(
    "--weights",
    type=float,
    nargs="+",
    help="the weights used for the weights combination type",
)
# TODO: add weighted combination

args = parser.parse_args()

# validate weights if this combination type is chosen
if args.combination == CombinationType.weights:
    if len(args.weights) != len(args.directories):
        raise ValueError(
            f"Number of weights {args.weights} does not match number of directories {args.directories}"
        )

for filename in tqdm(os.listdir(args.directories[0])):
    sams = map(lambda _dir: os.path.join(_dir, filename), args.directories)
    sams = map(lambda sam_file: cv.imread(sam_file, cv.IMREAD_GRAYSCALE), sams)
    sams = map(lambda sam: sam.astype(np.int), sams)
    sams = list(sams)

    for sam, _dir in zip(sams, args.directories):
        if sam is not None:
            continue

        raise RuntimeError(
            f"Could not produce combination for {filename} because it is missing in the directory {_dir}"
        )

    if args.combination == CombinationType.average:
        sam = reduce(lambda x, y: x + y, sams)
        sam = sam / len(sams)
    elif args.combination == CombinationType.maximum:
        sam = reduce(np.maximum, sams)
    elif args.combination == CombinationType.weights:
        sams = map(lambda t: t[0] * t[1], zip(args.weights, sams))
        sam = reduce(lambda x, y: x + y, sams)
    else:
        raise NotImplementedError(
            "CombinationType {args.combination} not yet implemented"
        )

    sam = sam.astype(np.uint8)
    cv.imwrite(os.path.join(args.output_dir, filename), sam)
