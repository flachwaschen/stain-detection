import torch

from stain_detection.models.cpd import CPD
from stain_detection.models.u2net import U2Net
from stain_detection.models.basnet import BASNet
from stain_detection.models.ldfnet import LDFNet
from stain_detection.models.poolnet import PoolNet

models = {"cpd": CPD, "u2": U2Net, "bas": BASNet, "ldf": LDFNet, "pool": PoolNet}


def load_model(model_name, weight_file=None, device=None, **kwargs):
    if model_name not in models:
        raise ValueError(f"Unkown model {model_name}")

    model_class = models[model_name]
    model = model_class(**kwargs)

    _device = (
        device
        if device is not None
        else torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    )
    if weight_file is not None:
        _weights = torch.load(weight_file, map_location=_device)
        model.load_state_dict(_weights)
    return model.to(_device)


def add_args(parser):
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        required=True,
        choices=models.keys(),
        help="the model with which to compute the salience maps",
    )
    parser.add_argument(
        "-w",
        "--weights",
        type=str,
        required=True,
        help="the file to restore weights from",
    )
    parser.add_argument(
        "-d",
        "--device",
        type=str,
        default="cuda:0" if torch.cuda.is_available() else "cpu",
        help="the device on which the model is loaded",
    )
