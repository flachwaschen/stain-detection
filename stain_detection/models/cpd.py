import cv2 as cv
import numpy as np
from PIL import Image
import scipy.stats as st

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import torchvision
import torchvision.transforms.functional as TF


def gkern(kernlen=16, nsig=3):
    interval = (2 * nsig + 1.0) / kernlen
    x = np.linspace(-nsig - interval / 2.0, nsig + interval / 2.0, kernlen + 1)
    kern1d = np.diff(st.norm.cdf(x))
    kernel_raw = np.sqrt(np.outer(kern1d, kern1d))
    kernel = kernel_raw / kernel_raw.sum()
    return kernel


def min_max_norm(in_):
    max_ = in_.max(3)[0].max(2)[0].unsqueeze(2).unsqueeze(3).expand_as(in_)
    min_ = in_.min(3)[0].min(2)[0].unsqueeze(2).unsqueeze(3).expand_as(in_)
    in_ = in_ - min_
    return in_.div(max_ - min_ + 1e-8)


class HA(nn.Module):
    # holistic attention module
    def __init__(self):
        super(HA, self).__init__()
        gaussian_kernel = np.float32(gkern(31, 4))
        gaussian_kernel = gaussian_kernel[np.newaxis, np.newaxis, ...]
        self.gaussian_kernel = Parameter(torch.from_numpy(gaussian_kernel))

    def forward(self, attention, x):
        soft_attention = F.conv2d(attention, self.gaussian_kernel, padding=15)
        soft_attention = min_max_norm(soft_attention)
        x = torch.mul(x, soft_attention.max(attention))
        return x


class B2_VGG(nn.Module):
    # VGG16 with two branches
    # pooling layer at the front of block
    def __init__(self, restore_weights=True):
        super(B2_VGG, self).__init__()
        conv1 = nn.Sequential()
        conv1.add_module("conv1_1", nn.Conv2d(3, 64, 3, 1, 1))
        conv1.add_module("relu1_1", nn.ReLU(inplace=True))
        conv1.add_module("conv1_2", nn.Conv2d(64, 64, 3, 1, 1))
        conv1.add_module("relu1_2", nn.ReLU(inplace=True))

        self.conv1 = conv1
        conv2 = nn.Sequential()
        conv2.add_module("pool1", nn.AvgPool2d(2, stride=2))
        conv2.add_module("conv2_1", nn.Conv2d(64, 128, 3, 1, 1))
        conv2.add_module("relu2_1", nn.ReLU())
        conv2.add_module("conv2_2", nn.Conv2d(128, 128, 3, 1, 1))
        conv2.add_module("relu2_2", nn.ReLU())
        self.conv2 = conv2

        conv3 = nn.Sequential()
        conv3.add_module("pool2", nn.AvgPool2d(2, stride=2))
        conv3.add_module("conv3_1", nn.Conv2d(128, 256, 3, 1, 1))
        conv3.add_module("relu3_1", nn.ReLU())
        conv3.add_module("conv3_2", nn.Conv2d(256, 256, 3, 1, 1))
        conv3.add_module("relu3_2", nn.ReLU())
        conv3.add_module("conv3_3", nn.Conv2d(256, 256, 3, 1, 1))
        conv3.add_module("relu3_3", nn.ReLU())
        self.conv3 = conv3

        conv4_1 = nn.Sequential()
        conv4_1.add_module("pool3_1", nn.AvgPool2d(2, stride=2))
        conv4_1.add_module("conv4_1_1", nn.Conv2d(256, 512, 3, 1, 1))
        conv4_1.add_module("relu4_1_1", nn.ReLU())
        conv4_1.add_module("conv4_2_1", nn.Conv2d(512, 512, 3, 1, 1))
        conv4_1.add_module("relu4_2_1", nn.ReLU())
        conv4_1.add_module("conv4_3_1", nn.Conv2d(512, 512, 3, 1, 1))
        conv4_1.add_module("relu4_3_1", nn.ReLU())
        self.conv4_1 = conv4_1

        conv5_1 = nn.Sequential()
        conv5_1.add_module("pool4_1", nn.AvgPool2d(2, stride=2))
        conv5_1.add_module("conv5_1_1", nn.Conv2d(512, 512, 3, 1, 1))
        conv5_1.add_module("relu5_1_1", nn.ReLU())
        conv5_1.add_module("conv5_2_1", nn.Conv2d(512, 512, 3, 1, 1))
        conv5_1.add_module("relu5_2_1", nn.ReLU())
        conv5_1.add_module("conv5_3_1", nn.Conv2d(512, 512, 3, 1, 1))
        conv5_1.add_module("relu5_3_1", nn.ReLU())
        self.conv5_1 = conv5_1

        conv4_2 = nn.Sequential()
        conv4_2.add_module("pool3_2", nn.AvgPool2d(2, stride=2))
        conv4_2.add_module("conv4_1_2", nn.Conv2d(256, 512, 3, 1, 1))
        conv4_2.add_module("relu4_1_2", nn.ReLU())
        conv4_2.add_module("conv4_2_2", nn.Conv2d(512, 512, 3, 1, 1))
        conv4_2.add_module("relu4_2_2", nn.ReLU())
        conv4_2.add_module("conv4_3_2", nn.Conv2d(512, 512, 3, 1, 1))
        conv4_2.add_module("relu4_3_2", nn.ReLU())
        self.conv4_2 = conv4_2

        conv5_2 = nn.Sequential()
        conv5_2.add_module("pool4_2", nn.AvgPool2d(2, stride=2))
        conv5_2.add_module("conv5_1_2", nn.Conv2d(512, 512, 3, 1, 1))
        conv5_2.add_module("relu5_1_2", nn.ReLU())
        conv5_2.add_module("conv5_2_2", nn.Conv2d(512, 512, 3, 1, 1))
        conv5_2.add_module("relu5_2_2", nn.ReLU())
        conv5_2.add_module("conv5_3_2", nn.Conv2d(512, 512, 3, 1, 1))
        conv5_2.add_module("relu5_3_2", nn.ReLU())
        self.conv5_2 = conv5_2

        if restore_weights:
            pre_train = torchvision.models.vgg16(pretrained=True).state_dict()
            self._initialize_weights(pre_train)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x1 = self.conv4_1(x)
        x1 = self.conv5_1(x1)
        x2 = self.conv4_2(x)
        x2 = self.conv5_2(x2)
        return x1, x2

    def _initialize_weights(self, pre_train):
        keys = list(pre_train.keys())
        self.conv1.conv1_1.weight.data.copy_(pre_train[keys[0]])
        self.conv1.conv1_2.weight.data.copy_(pre_train[keys[2]])
        self.conv2.conv2_1.weight.data.copy_(pre_train[keys[4]])
        self.conv2.conv2_2.weight.data.copy_(pre_train[keys[6]])
        self.conv3.conv3_1.weight.data.copy_(pre_train[keys[8]])
        self.conv3.conv3_2.weight.data.copy_(pre_train[keys[10]])
        self.conv3.conv3_3.weight.data.copy_(pre_train[keys[12]])
        self.conv4_1.conv4_1_1.weight.data.copy_(pre_train[keys[14]])
        self.conv4_1.conv4_2_1.weight.data.copy_(pre_train[keys[16]])
        self.conv4_1.conv4_3_1.weight.data.copy_(pre_train[keys[18]])
        self.conv5_1.conv5_1_1.weight.data.copy_(pre_train[keys[20]])
        self.conv5_1.conv5_2_1.weight.data.copy_(pre_train[keys[22]])
        self.conv5_1.conv5_3_1.weight.data.copy_(pre_train[keys[24]])
        self.conv4_2.conv4_1_2.weight.data.copy_(pre_train[keys[14]])
        self.conv4_2.conv4_2_2.weight.data.copy_(pre_train[keys[16]])
        self.conv4_2.conv4_3_2.weight.data.copy_(pre_train[keys[18]])
        self.conv5_2.conv5_1_2.weight.data.copy_(pre_train[keys[20]])
        self.conv5_2.conv5_2_2.weight.data.copy_(pre_train[keys[22]])
        self.conv5_2.conv5_3_2.weight.data.copy_(pre_train[keys[24]])

        self.conv1.conv1_1.bias.data.copy_(pre_train[keys[1]])
        self.conv1.conv1_2.bias.data.copy_(pre_train[keys[3]])
        self.conv2.conv2_1.bias.data.copy_(pre_train[keys[5]])
        self.conv2.conv2_2.bias.data.copy_(pre_train[keys[7]])
        self.conv3.conv3_1.bias.data.copy_(pre_train[keys[9]])
        self.conv3.conv3_2.bias.data.copy_(pre_train[keys[11]])
        self.conv3.conv3_3.bias.data.copy_(pre_train[keys[13]])
        self.conv4_1.conv4_1_1.bias.data.copy_(pre_train[keys[15]])
        self.conv4_1.conv4_2_1.bias.data.copy_(pre_train[keys[17]])
        self.conv4_1.conv4_3_1.bias.data.copy_(pre_train[keys[19]])
        self.conv5_1.conv5_1_1.bias.data.copy_(pre_train[keys[21]])
        self.conv5_1.conv5_2_1.bias.data.copy_(pre_train[keys[23]])
        self.conv5_1.conv5_3_1.bias.data.copy_(pre_train[keys[25]])
        self.conv4_2.conv4_1_2.bias.data.copy_(pre_train[keys[15]])
        self.conv4_2.conv4_2_2.bias.data.copy_(pre_train[keys[17]])
        self.conv4_2.conv4_3_2.bias.data.copy_(pre_train[keys[19]])
        self.conv5_2.conv5_1_2.bias.data.copy_(pre_train[keys[21]])
        self.conv5_2.conv5_2_2.bias.data.copy_(pre_train[keys[23]])
        self.conv5_2.conv5_3_2.bias.data.copy_(pre_train[keys[25]])


class RFB(nn.Module):
    def __init__(self, in_channel, out_channel):
        super(RFB, self).__init__()
        self.relu = nn.ReLU(True)
        self.branch0 = nn.Sequential(
            nn.Conv2d(in_channel, out_channel, 1),
        )
        self.branch1 = nn.Sequential(
            nn.Conv2d(in_channel, out_channel, 1),
            nn.Conv2d(out_channel, out_channel, kernel_size=(1, 3), padding=(0, 1)),
            nn.Conv2d(out_channel, out_channel, kernel_size=(3, 1), padding=(1, 0)),
            nn.Conv2d(out_channel, out_channel, 3, padding=3, dilation=3),
        )
        self.branch2 = nn.Sequential(
            nn.Conv2d(in_channel, out_channel, 1),
            nn.Conv2d(out_channel, out_channel, kernel_size=(1, 5), padding=(0, 2)),
            nn.Conv2d(out_channel, out_channel, kernel_size=(5, 1), padding=(2, 0)),
            nn.Conv2d(out_channel, out_channel, 3, padding=5, dilation=5),
        )
        self.branch3 = nn.Sequential(
            nn.Conv2d(in_channel, out_channel, 1),
            nn.Conv2d(out_channel, out_channel, kernel_size=(1, 7), padding=(0, 3)),
            nn.Conv2d(out_channel, out_channel, kernel_size=(7, 1), padding=(3, 0)),
            nn.Conv2d(out_channel, out_channel, 3, padding=7, dilation=7),
        )
        self.conv_cat = nn.Conv2d(4 * out_channel, out_channel, 3, padding=1)
        self.conv_res = nn.Conv2d(in_channel, out_channel, 1)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                m.weight.data.normal_(std=0.01)
                m.bias.data.fill_(0)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        x_cat = torch.cat((x0, x1, x2, x3), 1)
        x_cat = self.conv_cat(x_cat)

        x = self.relu(x_cat + self.conv_res(x))
        return x


class aggregation(nn.Module):
    def __init__(self, channel):
        super(aggregation, self).__init__()
        self.relu = nn.ReLU(True)

        self.upsample = nn.Upsample(scale_factor=2, mode="bilinear", align_corners=True)
        self.conv_upsample1 = nn.Conv2d(channel, channel, 3, padding=1)
        self.conv_upsample2 = nn.Conv2d(channel, channel, 3, padding=1)
        self.conv_upsample3 = nn.Conv2d(channel, channel, 3, padding=1)
        self.conv_upsample4 = nn.Conv2d(channel, channel, 3, padding=1)
        self.conv_upsample5 = nn.Conv2d(2 * channel, 2 * channel, 3, padding=1)

        self.conv_concat2 = nn.Conv2d(2 * channel, 2 * channel, 3, padding=1)
        self.conv_concat3 = nn.Conv2d(3 * channel, 3 * channel, 3, padding=1)
        self.conv4 = nn.Conv2d(3 * channel, 3 * channel, 3, padding=1)
        self.conv5 = nn.Conv2d(3 * channel, 1, 1)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                m.weight.data.normal_(std=0.01)
                m.bias.data.fill_(0)

    def forward(self, x1, x2, x3):
        # x1: 1/16 x2: 1/8 x3: 1/4
        x1_1 = x1
        x2_1 = self.conv_upsample1(self.upsample(x1)) * x2
        x3_1 = (
            self.conv_upsample2(self.upsample(self.upsample(x1)))
            * self.conv_upsample3(self.upsample(x2))
            * x3
        )

        x2_2 = torch.cat((x2_1, self.conv_upsample4(self.upsample(x1_1))), 1)
        x2_2 = self.conv_concat2(x2_2)

        x3_2 = torch.cat((x3_1, self.conv_upsample5(self.upsample(x2_2))), 1)
        x3_2 = self.conv_concat3(x3_2)

        x = self.conv4(x3_2)
        x = self.conv5(x)

        return x


class CPD(nn.Module):
    def __init__(self, init_vgg=True, channel=32, input_size=352):
        super().__init__()
        self.vgg = B2_VGG(restore_weights=init_vgg)
        self.rfb3_1 = RFB(256, channel)
        self.rfb4_1 = RFB(512, channel)
        self.rfb5_1 = RFB(512, channel)
        self.agg1 = aggregation(channel)

        self.rfb3_2 = RFB(256, channel)
        self.rfb4_2 = RFB(512, channel)
        self.rfb5_2 = RFB(512, channel)
        self.agg2 = aggregation(channel)

        self.HA = HA()
        self.upsample = nn.Upsample(
            scale_factor=4, mode="bilinear", align_corners=False
        )

        self.input_size = input_size
        self.loss_func = torch.nn.BCEWithLogitsLoss()

    def forward(self, x):
        x1 = self.vgg.conv1(x)
        x2 = self.vgg.conv2(x1)
        x3 = self.vgg.conv3(x2)

        x3_1 = x3
        x4_1 = self.vgg.conv4_1(x3_1)
        x5_1 = self.vgg.conv5_1(x4_1)
        x3_1 = self.rfb3_1(x3_1)
        x4_1 = self.rfb4_1(x4_1)
        x5_1 = self.rfb5_1(x5_1)
        attention = self.agg1(x5_1, x4_1, x3_1)

        x3_2 = self.HA(attention.sigmoid(), x3)
        x4_2 = self.vgg.conv4_2(x3_2)
        x5_2 = self.vgg.conv5_2(x4_2)
        x3_2 = self.rfb3_2(x3_2)
        x4_2 = self.rfb4_2(x4_2)
        x5_2 = self.rfb5_2(x5_2)
        detection = self.agg2(x5_2, x4_2, x3_2)

        return {
            "attention": self.upsample(attention),
            "detection": self.upsample(detection),
        }

    def compute_loss(self, outputs, labels):
        loss_atts = self.loss_func(outputs["attention"], labels)
        loss_dets = self.loss_func(outputs["detection"], labels)
        return loss_atts + loss_dets

    def salience_map(self, outputs):
        return outputs["detection"].sigmoid().data.squeeze()

    def preprocess_image(self, image):
        """
        Pre-process an image as a numpy array as loaded by OpenCV to a
        torch tensor which can be processed by the model.
        """
        # change channel order from BGR to RGB
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)

        # convert to pil image
        image = TF.to_pil_image(image)

        # resize
        image = TF.resize(image, (self.input_size, self.input_size))

        # convert to tensor
        image = TF.to_tensor(image)

        # normalize
        image = TF.normalize(
            image, mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
        )
        return image

    def preprocess_label(self, label):
        """
        Pre-process a label as a numpy array as loaded by OpenCV to a
        torch tensor which is used for the loss computation.
        """
        # convert to pil image
        label = TF.to_pil_image(label)
        # resize
        label = TF.resize(label, (self.input_size, self.input_size))
        # convert to tensor (this also sets range to [0, 1])
        label = TF.to_tensor(label)
        return label

    def init_optimizer(self):
        return torch.optim.Adam(self.parameters(), lr=1e-4)
