#!/usr/bin/python3
# coding=utf-8

import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F


class Bottleneck(nn.Module):
    def __init__(self, inplanes, planes, stride=1, downsample=None, dilation=1):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(
            planes,
            planes,
            kernel_size=3,
            stride=stride,
            padding=(3 * dilation - 1) // 2,
            bias=False,
            dilation=dilation,
        )
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * 4, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * 4)
        self.downsample = downsample

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)), inplace=True)
        out = F.relu(self.bn2(self.conv2(out)), inplace=True)
        out = self.bn3(self.conv3(out))
        if self.downsample is not None:
            x = self.downsample(x)
        return F.relu(out + x, inplace=True)


class ResNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.inplanes = 64
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.layer1 = self.make_layer(64, 3, stride=1, dilation=1)
        self.layer2 = self.make_layer(128, 4, stride=2, dilation=1)
        self.layer3 = self.make_layer(256, 6, stride=2, dilation=1)
        self.layer4 = self.make_layer(512, 3, stride=2, dilation=1)

    def make_layer(self, planes, blocks, stride, dilation):
        downsample = nn.Sequential(
            nn.Conv2d(
                self.inplanes, planes * 4, kernel_size=1, stride=stride, bias=False
            ),
            nn.BatchNorm2d(planes * 4),
        )
        layers = [
            Bottleneck(self.inplanes, planes, stride, downsample, dilation=dilation)
        ]
        self.inplanes = planes * 4
        for _ in range(1, blocks):
            layers.append(Bottleneck(self.inplanes, planes, dilation=dilation))
        return nn.Sequential(*layers)

    def forward(self, x):
        out1 = F.relu(self.bn1(self.conv1(x)), inplace=True)
        out1 = F.max_pool2d(out1, kernel_size=3, stride=2, padding=1)
        out2 = self.layer1(out1)
        out3 = self.layer2(out2)
        out4 = self.layer3(out3)
        out5 = self.layer4(out4)
        return out1, out2, out3, out4, out5


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()
        self.conv0 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn0 = nn.BatchNorm2d(64)
        self.conv1 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn1 = nn.BatchNorm2d(64)
        self.conv2 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn2 = nn.BatchNorm2d(64)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn3 = nn.BatchNorm2d(64)

    def forward(self, input1, input2=[0, 0, 0, 0]):
        out0 = F.relu(self.bn0(self.conv0(input1[0] + input2[0])), inplace=True)
        out0 = F.interpolate(
            out0, size=input1[1].size()[2:], mode="bilinear", align_corners=False
        )
        out1 = F.relu(self.bn1(self.conv1(input1[1] + input2[1] + out0)), inplace=True)
        out1 = F.interpolate(
            out1, size=input1[2].size()[2:], mode="bilinear", align_corners=False
        )
        out2 = F.relu(self.bn2(self.conv2(input1[2] + input2[2] + out1)), inplace=True)
        out2 = F.interpolate(
            out2, size=input1[3].size()[2:], mode="bilinear", align_corners=False
        )
        out3 = F.relu(self.bn3(self.conv3(input1[3] + input2[3] + out2)), inplace=True)
        return out3


class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()
        self.conv1 = nn.Conv2d(128, 64, kernel_size=3, stride=1, padding=1)
        self.bn1 = nn.BatchNorm2d(64)
        self.conv2 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn2 = nn.BatchNorm2d(64)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn3 = nn.BatchNorm2d(64)
        self.conv4 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn4 = nn.BatchNorm2d(64)

        self.conv1b = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn1b = nn.BatchNorm2d(64)
        self.conv2b = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn2b = nn.BatchNorm2d(64)
        self.conv3b = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn3b = nn.BatchNorm2d(64)
        self.conv4b = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn4b = nn.BatchNorm2d(64)

        self.conv1d = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn1d = nn.BatchNorm2d(64)
        self.conv2d = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn2d = nn.BatchNorm2d(64)
        self.conv3d = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn3d = nn.BatchNorm2d(64)
        self.conv4d = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.bn4d = nn.BatchNorm2d(64)

    def forward(self, out1):
        out1 = F.relu(self.bn1(self.conv1(out1)), inplace=True)
        out2 = F.max_pool2d(out1, kernel_size=2, stride=2)
        out2 = F.relu(self.bn2(self.conv2(out2)), inplace=True)
        out3 = F.max_pool2d(out2, kernel_size=2, stride=2)
        out3 = F.relu(self.bn3(self.conv3(out3)), inplace=True)
        out4 = F.max_pool2d(out3, kernel_size=2, stride=2)
        out4 = F.relu(self.bn4(self.conv4(out4)), inplace=True)

        out1b = F.relu(self.bn1b(self.conv1b(out1)), inplace=True)
        out2b = F.relu(self.bn2b(self.conv2b(out2)), inplace=True)
        out3b = F.relu(self.bn3b(self.conv3b(out3)), inplace=True)
        out4b = F.relu(self.bn4b(self.conv4b(out4)), inplace=True)

        out1d = F.relu(self.bn1d(self.conv1d(out1)), inplace=True)
        out2d = F.relu(self.bn2d(self.conv2d(out2)), inplace=True)
        out3d = F.relu(self.bn3d(self.conv3d(out3)), inplace=True)
        out4d = F.relu(self.bn4d(self.conv4d(out4)), inplace=True)
        return (out4b, out3b, out2b, out1b), (out4d, out3d, out2d, out1d)


def iou_loss(pred, mask):
    pred = torch.sigmoid(pred)
    inter = (pred * mask).sum(dim=(2, 3))
    union = (pred + mask).sum(dim=(2, 3))
    iou = 1 - (inter + 1) / (union - inter + 1)
    return iou.mean()


class LDFNet(nn.Module):
    def __init__(self, input_size=352):
        super().__init__()
        self.bkbone = ResNet()
        self.conv5b = nn.Sequential(
            nn.Conv2d(2048, 64, kernel_size=1),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
        )
        self.conv4b = nn.Sequential(
            nn.Conv2d(1024, 64, kernel_size=1),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
        )
        self.conv3b = nn.Sequential(
            nn.Conv2d(512, 64, kernel_size=1),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
        )
        self.conv2b = nn.Sequential(
            nn.Conv2d(256, 64, kernel_size=1),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
        )

        self.conv5d = nn.Sequential(
            nn.Conv2d(2048, 64, kernel_size=1),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
        )
        self.conv4d = nn.Sequential(
            nn.Conv2d(1024, 64, kernel_size=1),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
        )
        self.conv3d = nn.Sequential(
            nn.Conv2d(512, 64, kernel_size=1),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
        )
        self.conv2d = nn.Sequential(
            nn.Conv2d(256, 64, kernel_size=1),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
        )

        self.encoder = Encoder()
        self.decoderb = Decoder()
        self.decoderd = Decoder()
        self.linearb = nn.Conv2d(64, 1, kernel_size=3, padding=1)
        self.lineard = nn.Conv2d(64, 1, kernel_size=3, padding=1)
        self.linear = nn.Sequential(
            nn.Conv2d(128, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 1, kernel_size=3, padding=1),
        )

        self.input_size = input_size

    # def forward(self, x, shape=None):
    def forward(self, x):
        out1, out2, out3, out4, out5 = self.bkbone(x)
        out2b, out3b, out4b, out5b = (
            self.conv2b(out2),
            self.conv3b(out3),
            self.conv4b(out4),
            self.conv5b(out5),
        )
        out2d, out3d, out4d, out5d = (
            self.conv2d(out2),
            self.conv3d(out3),
            self.conv4d(out4),
            self.conv5d(out5),
        )

        outb1 = self.decoderb([out5b, out4b, out3b, out2b])
        outd1 = self.decoderd([out5d, out4d, out3d, out2d])
        out1 = torch.cat([outb1, outd1], dim=1)
        outb2, outd2 = self.encoder(out1)
        outb2 = self.decoderb([out5b, out4b, out3b, out2b], outb2)
        outd2 = self.decoderd([out5d, out4d, out3d, out2d], outd2)
        out2 = torch.cat([outb2, outd2], dim=1)

        shape = x.size()[2:]
        out1 = F.interpolate(
            self.linear(out1), size=shape, mode="bilinear", align_corners=False
        )
        outb1 = F.interpolate(
            self.linearb(outb1), size=shape, mode="bilinear", align_corners=False
        )
        outd1 = F.interpolate(
            self.lineard(outd1), size=shape, mode="bilinear", align_corners=False
        )

        out2 = F.interpolate(
            self.linear(out2), size=shape, mode="bilinear", align_corners=False
        )
        outb2 = F.interpolate(
            self.linearb(outb2), size=shape, mode="bilinear", align_corners=False
        )
        outd2 = F.interpolate(
            self.lineard(outd2), size=shape, mode="bilinear", align_corners=False
        )
        return outb1, outd1, out1, outb2, outd2, out2

    def compute_loss(self, outputs, labels):
        labels, labels_body, labels_detail = labels

        loss = 0
        loss += F.binary_cross_entropy_with_logits(outputs[0], labels_body)
        loss += F.binary_cross_entropy_with_logits(outputs[1], labels_detail)
        loss += F.binary_cross_entropy_with_logits(outputs[2], labels)
        loss += iou_loss(outputs[2], labels)
        loss += F.binary_cross_entropy_with_logits(outputs[3], labels_body)
        loss += F.binary_cross_entropy_with_logits(outputs[4], labels_detail)
        loss += F.binary_cross_entropy_with_logits(outputs[5], labels)
        loss += iou_loss(outputs[5], labels)

        return loss / 2.0

    def salience_map(self, outputs):
        return torch.sigmoid(outputs[-1].squeeze())

    def preprocess_image(self, image: np.array):
        """
        Pre-process an image as a numpy array as loaded by OpenCV to a
        torch tensor which can be processed by the model.
        """
        # change channel order from BGR to RGB
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)

        # normalize
        mean = np.array([[[124.55, 118.90, 102.94]]])
        std = np.array([[[56.77, 55.97, 57.50]]])
        image = (image - mean) / std

        # resize
        image = cv.resize(
            image, (self.input_size, self.input_size), interpolation=cv.INTER_LINEAR
        )

        # convert to torch Tensor
        image = torch.from_numpy(image)

        # reorder from (h x w x c) to (c x h x w)
        image = image.permute(2, 0, 1)
        return image.type(torch.FloatTensor)

    def preprocess_label(self, label):
        """
        Pre-process a label as a numpy array as loaded by OpenCV to a
        torch tensor which is used for the loss computation.
        """
        # compute body label
        label_body = cv.blur(label, ksize=(5, 5))
        label_body = cv.distanceTransform(
            label_body, distanceType=cv.DIST_L2, maskSize=5
        )
        label_body = label_body ** 0.5

        tmp = label_body[np.where(label_body > 0)]
        if len(tmp) != 0:
            label_body[np.where(label_body > 0)] = np.floor(tmp / np.max(tmp) * 255)

        # compute detail label
        label_detail = label - label_body
        label_detail = np.where(label_detail < 0, 0, label_detail)

        # recombine as label
        label = (label, label_body, label_detail)

        # resize
        label = map(
            lambda l: cv.resize(
                l, (self.input_size, self.input_size), interpolation=cv.INTER_LINEAR
            ),
            label,
        )

        # normalize
        label = map(lambda l: l / 255.0, label)

        # convert to tensor
        label = map(lambda l: torch.from_numpy(l), label)
        label = map(lambda l: l.unsqueeze(dim=0), label)

        return tuple(label)

    def init_optimizer(self):
        base, head = [], []
        for name, param in self.named_parameters():
            if 'bkbone.conv1' in name or 'bkbone.bn1' in name:
                pass
            elif 'bkbone' in name:
                base.append(param)
            else:
                head.append(param)
        return torch.optim.SGD([{'params':base}, {'params':head}], lr=0.05, momentum=0.9, weight_decay=5e-4, nesterov=True)


