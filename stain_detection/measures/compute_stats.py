import argparse
import os

import cv2 as cv
import pandas as pd
import tqdm

from stain_detection.dataset.split import read_ids, get_laundry_id
from stain_detection.measures.pixel_wise import PixelwiseQuantities
from stain_detection.measures.region_wise import RegionQuantities
from stain_detection.measures.util import connect_regions, compute_connected_components

parser = argparse.ArgumentParser(
    "Compute measures", formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
# data
parser.add_argument(
    "--predictions_dir",
    type=str,
    required=True,
    help="directory containing predictions",
)
parser.add_argument(
    "--labels_dir", type=str, required=True, help="directory containing labels"
)
parser.add_argument(
    "--laundry_ids",
    type=str,
    help="optional file of laundry ids used to filter the files",
)
parser.add_argument(
    "--output", type=str, help="optional output file to which results are written"
)
parser.add_argument(
    "-f",
    "--force",
    action="store_true",
    help="if output already exists delete it without asking",
)

# threshold
parser.add_argument(
    "-t",
    "--threshold",
    type=int,
    default=100,
    help="the threshold to apply to the prediction before measures are computed",
)
parser.add_argument(
    "-e",
    "--threshold_end",
    type=int,
    default=-1,
    help="if higher than threshold start iterating over thresholds",
)
parser.add_argument(
    "-s",
    "--threshold_step",
    type=int,
    default=1,
    help="it threshold_end is higher than threshold start iterating and increase the threshold by this value",
)
parser.add_argument(
    "--iterate",
    action="store_true",
    help="equivalent to setting threshold=0, threshold_end=255, threshold_step=10",
)
# pre-processing
parser.add_argument(
    "--connect_regions",
    action="store_true",
    help="connection regions in the prediction mapping to the same labeled region",
)

parser.add_argument(
    "--region_threshold",
    type=float,
    default=RegionQuantities.computeFromCcs.__func__.__defaults__[0],
    help="the region threshold to compute correctly detected regions",
)
parser.add_argument(
    "--prediction_threshold",
    type=float,
    default=RegionQuantities.computeFromCcs.__func__.__defaults__[1],
    help="the prediction threshold to compute valid predictions",
)

args = parser.parse_args()
kwargs = vars(args).copy()

if args.iterate:
    args.threshold = 0
    args.threshold_end = 255
    args.threshold_step = 10

if args.threshold_end <= args.threshold:
    args.threshold_end = args.threshold + 1


files = os.listdir(args.predictions_dir)
files = list(filter(lambda f: f.endswith(".png"), files))
if args.laundry_ids:
    laundry_ids = read_ids(args.laundry_ids)
    files = filter(lambda f: get_laundry_id(f) in laundry_ids, files)
    files = list(files)
files.sort()

pq = PixelwiseQuantities()
rq = RegionQuantities()
keys = ["filename", "threshold", *pq.to_dict().keys(), *rq.to_dict().keys()]
_data = dict(map(lambda k: (k, []), keys))
data = pd.DataFrame(_data)

max_file_length = map(lambda f: len(f), files)
max_file_length = max(max_file_length)
for filename in files:
    label_const      = cv.imread(os.path.join(args.labels_dir, filename), cv.IMREAD_GRAYSCALE)
    prediction_const = cv.imread(
        os.path.join(args.predictions_dir, filename), cv.IMREAD_GRAYSCALE
    )

    _data = {"filename": filename}
    _range = range(args.threshold, args.threshold_end, args.threshold_step)
    for t in _range:
        print(f"File: {filename:>{max_file_length}} - Threshold {str(t):>{len(str(args.threshold_end))}}/{max(_range)}", end="\r")
        _data["threshold"] = t

        label = label_const.copy()
        _, prediction = cv.threshold(prediction_const, t, 255, cv.THRESH_BINARY)

        ccs_pre = compute_connected_components(prediction)
        ccs_lab = compute_connected_components(label)

        if args.connect_regions:
            prediction, label = connect_regions(
                prediction.copy(), label.copy(), ccs_pre=ccs_pre, ccs_ann=ccs_lab
            )

        pq = PixelwiseQuantities.compute(prediction, label)
        rq = RegionQuantities.computeFromCcs(ccs_pre, ccs_lab, **kwargs)

        _data.update(pq.to_dict())
        _data.update(rq.to_dict())

        data = data.append(_data, ignore_index=True)

for key in filter(lambda k: k != "filename", keys):
    data[key] = data[key].astype(int)

data.to_csv(args.output, index=False)
