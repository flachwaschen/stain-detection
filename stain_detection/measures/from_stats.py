import argparse

import pandas as pd

import stain_detection.measures.pixel_wise as pixel_wise
import stain_detection.measures.region_wise as region_wise

parser = argparse.ArgumentParser(
    "Compute measures", formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument("stats", type=str)
# measure parameters
parser.add_argument(
    "--false_prediction_weight",
    type=float,
    default=region_wise.rdr.__defaults__[0],
    help="the false prediction weight to compute the rdr",
)
parser.add_argument(
    "--beta_squared",
    type=float,
    default=pixel_wise.f_beta.__defaults__[0],
    help="the beta squared parameter to compute f beta",
)
# parsing
args = parser.parse_args()
kwargs = vars(args).copy()

stats = pd.read_csv(args.stats)

headers = ["threshold", "precision", "recall", "iou", "f_beta", "rdr"]
lenghtes = list(map(lambda h: max(6, len(h)), headers))
headers = map(lambda t: f"{t[0]:>{t[1]}}", zip(headers, lenghtes))
print(" ".join(headers))
for t in sorted(stats["threshold"].unique()):
    _stats = stats[stats["threshold"] == t]
    _stats = _stats.sum()
    
    pq = pixel_wise.PixelwiseQuantities.from_dict(_stats)
    rq = region_wise.RegionQuantities.from_dict(_stats)

    precision = pixel_wise.precision(pq)
    recall = pixel_wise.recall(pq)
    iou = pixel_wise.iou(pq)
    f_beta = pixel_wise.f_beta(pq, **kwargs)
    rdr = region_wise.rdr(rq, **kwargs)

    values = [t, precision, recall, iou, f_beta, rdr]
    values = map(lambda v: f"{v:.4f}" if isinstance(v, float) else f"{v}", values)
    values = map(lambda t: f"{t[0]:>{t[1]}}", zip(values, lenghtes))
    print(" ".join(values))
