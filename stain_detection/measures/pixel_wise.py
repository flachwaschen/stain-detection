import numpy as np


class PixelwiseQuantities:

    __slots__ = ["tp", "tn", "fp", "fn"]

    def __init__(self, tp: int = 0, tn: int = 0, fp: int = 0, fn: int = 0):
        self.tp = tp
        self.tn = tn
        self.fp = fp
        self.fn = fn

    def __add__(self, other):
        tp = self.tp + other.tp
        tn = self.tn + other.tn
        fp = self.fp + other.fp
        fn = self.fn + other.fn
        return PixelwiseQuantities(tp, tn, fp, fn)

    def __repr__(self):
        l = [self.tp, self.tn, self.fp, self.fn]
        max_len = len(str(max([self.tp, self.tn, self.fp, self.fn])))
        s = sum(l)
        ls = [(x / s if s > 0 else 0) for x in l]
        return (
            f"[TP, FP] = [{self.tp:{max_len}d}, {self.fp:{max_len}d}] = [{ls[0]:.3f}, {ls[2]:.3f}]\n"
            + f"[TN, FN] = [{self.tn:{max_len}d}, {self.fn:{max_len}d}] = [{ls[1]:.3f}, {ls[3]:.3f}]"
        )

    def to_dict(self):
        return {
            "true_positives": self.tp,
            "true_negatives": self.tn,
            "false_positives": self.fp,
            "false_negatives": self.fn,
        }

    @classmethod
    def from_dict(cls, _dict):
        return cls(
            tp=_dict["true_positives"],
            tn=_dict["true_negatives"],
            fp=_dict["false_positives"],
            fn=_dict["false_negatives"],
        )

    @classmethod
    def compute(cls, prediction: np.array, label: np.array):
        prediction = prediction > 0
        label = label > 0

        intersection = np.logical_and(prediction, label)

        tp = intersection.sum()
        fp = prediction.sum() - tp
        fn = label.sum() - tp

        pixel_count = prediction.shape[0] * prediction.shape[1]
        tn = pixel_count - tp - fp - fn

        return cls(tp, tn, fp, fn)

    @classmethod
    def fromStr(cls, _str: str):
        tp, fp, tn, fn = list(map(lambda s: int(s.strip()), _str.split(",")))
        return cls(tp, fp, tn, fn)


def precision(q: PixelwiseQuantities, **kwargs):
    if q.tp + q.fp == 0:
        raise ValueError("No true positives and false positives")
    return q.tp / (q.tp + q.fp)


def recall(q: PixelwiseQuantities, **kwargs):
    if q.tp + q.fn == 0:
        raise ValueError("No true positives and false negatives")
    return q.tp / (q.tp + q.fn)


def iou(q: PixelwiseQuantities, **kwargs):
    if q.tp + q.fp + q.fn == 0:
        raise ValueError("No true positives, false positives and false negatives")
    return q.tp / (q.tp + q.fp + q.fn)


def f_beta(q: PixelwiseQuantities, beta_squared: float = 0.3, **kwargs):
    _precision = precision(q)
    _recall = recall(q)
    return ((1 + beta_squared) * _precision * _recall) / (
        beta_squared * _precision + _recall
    )


MEASURES = [precision, recall, iou, f_beta]


if __name__ == "__main__":
    import argparse
    import cv2 as cv

    parser = argparse.ArgumentParser(
        "Compute pixel-wise measures",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--prediction",
        type=str,
        required=True,
        help="the prediction to compute the measures for",
    )
    parser.add_argument(
        "--label",
        type=str,
        required=True,
        help="the label image file matching the prediction",
    )
    parser.add_argument(
        "-t",
        "--threshold",
        type=int,
        default=100,
        help="the threshold to apply to the prediction before measures are computed",
    )
    parser.add_argument(
        "--beta_squared",
        type=float,
        default=f_beta.__defaults__[0],
        help="the beta squared parameter to compute f beta",
    )
    args = parser.parse_args()
    args_dict = vars(args)

    label = cv.imread(args.label, cv.IMREAD_GRAYSCALE)
    prediction = cv.imread(args.prediction, cv.IMREAD_GRAYSCALE)
    _, prediction = cv.threshold(prediction, args.threshold, 255, cv.THRESH_BINARY)
    q = PixelwiseQuantities.compute(prediction, label)

    headers = list(map(lambda m: m.__name__, MEASURES))
    lenghtes = list(map(lambda h: max(6, len(h)), headers))

    headers = map(lambda t: f"{t[0]:>{t[1]}}", zip(headers, lenghtes))
    headers = " ".join(headers)
    print(headers)

    values = map(lambda m: m(q, **args_dict), MEASURES)
    values = map(lambda v: f"{v:.3f}", values)
    values = map(lambda t: f"{t[0]:>{t[1]}}", zip(values, lenghtes))
    values = " ".join(values)
    print(values)
