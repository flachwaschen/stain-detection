from typing import List

import cv2 as cv
import numpy as np

from stain_detection.measures.util import (
    ConnectedComponent,
    compute_connected_components,
)


class RegionQuantities:

    __slots__ = ["regions", "detected_regions", "false_predictions"]

    def __init__(
        self, regions: int = 0, detected_regions: int = 0, false_predictions: int = 0
    ):
        self.regions = regions
        self.detected_regions = detected_regions
        self.false_predictions = false_predictions

    def __repr__(self):
        return f"(Regions, Detections, False-Predictions) = ({self.regions}, {self.detected_regions}, {self.false_predictions})"

    def __add__(self, other):
        regions = self.regions + other.regions
        detected_regions = self.detected_regions + other.detected_regions
        false_predictions = self.false_predictions + other.false_predictions
        return RegionQuantities(regions, detected_regions, false_predictions)

    def to_dict(self):
        return {
            "regions": self.regions,
            "detected_regions": self.detected_regions,
            "false_predictions": self.false_predictions,
        }

    @classmethod
    def from_dict(cls, _dict):
        return cls(
            regions=_dict["regions"],
            detected_regions=_dict["detected_regions"],
            false_predictions=_dict["false_predictions"],
        )

    @classmethod
    def computeFromCcs(
        cls,
        ccs_pre: List[ConnectedComponent],
        ccs_ann: List[ConnectedComponent],
        region_threshold=0.5,
        prediction_threshold=0.25,
        detected_regions=None,
        false_predictions=None,
        **kwargs,
    ):
        predictionCoverage = dict(map(lambda cc: (cc.label, 0), ccs_pre))
        for cc_pre in ccs_pre:
            for cc_ann in ccs_ann:
                predictionCoverage[cc_pre.label] += (
                    cc_pre.intersection_count(cc_ann) / cc_pre.stats[cv.CC_STAT_AREA]
                )

        if false_predictions is None:
            false_predictions = []
        false_predictions.extend(
            [
                label
                for label in predictionCoverage
                if predictionCoverage[label] < prediction_threshold
            ]
        )

        annotationCoverage = dict(map(lambda cc: (cc.label, 0), ccs_ann))
        for cc_pre in ccs_pre:
            if cc_pre.label in false_predictions:
                continue

            for cc_ann in ccs_ann:
                annotationCoverage[cc_ann.label] += (
                    cc_ann.intersection_count(cc_pre) / cc_ann.stats[cv.CC_STAT_AREA]
                )

        if detected_regions is None:
            detected_regions = []
        detected_regions.extend(
            [
                label
                for label in annotationCoverage
                if annotationCoverage[label] >= region_threshold
            ]
        )

        return cls(len(ccs_ann), len(detected_regions), len(false_predictions))

    @classmethod
    def computeFromImgs(cls, prediction: np.array, label: np.array, **kwargs):
        ccs_pre = compute_connected_components(prediction)
        ccs_ann = compute_connected_components(label)
        return cls.computeFromCcs(ccs_pre, ccs_ann, **kwargs)

    @classmethod
    def fromStr(cls, _str: str):
        _str = _str.split("=")[1].strip()[1:-1]
        _str = _str.split(",")
        _str = map(lambda s: int(s.strip()), _str)
        return cls(*_str)


def rdr(q: RegionQuantities, false_prediction_weight=0.1, **kwargs):
    if q.regions == 0:
        raise ValueError(f"No regions in Quantities:\n{q}")
    return q.detected_regions / (
        q.regions + false_prediction_weight * q.false_predictions
    )


MEASURES = [rdr]

if __name__ == "__main__":
    import argparse
    import cv2 as cv

    parser = argparse.ArgumentParser(
        "Compute region-wise measures",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--prediction",
        type=str,
        required=True,
        help="the prediction to compute the measures for",
    )
    parser.add_argument(
        "--label",
        type=str,
        required=True,
        help="the label image file matching the prediction",
    )
    parser.add_argument(
        "-t",
        "--threshold",
        type=int,
        default=100,
        help="the threshold to apply to the prediction before measures are computed",
    )
    parser.add_argument(
        "--false_prediction_weight",
        type=float,
        default=rdr.__defaults__[0],
        help="the false prediction weight to compute the rdr",
    )
    parser.add_argument(
        "--region_threshold",
        type=float,
        default=RegionQuantities.computeFromCcs.__func__.__defaults__[0],
        help="the region threshold to compute correctly detected regions",
    )
    parser.add_argument(
        "--prediction_threshold",
        type=float,
        default=RegionQuantities.computeFromCcs.__func__.__defaults__[1],
        help="the prediction threshold to compute valid predictions",
    )
    args = parser.parse_args()
    args_dict = vars(args).copy()
    del args_dict["threshold"]
    del args_dict["prediction"]
    del args_dict["label"]

    label = cv.imread(args.label, cv.IMREAD_GRAYSCALE)
    prediction = cv.imread(args.prediction, cv.IMREAD_GRAYSCALE)
    _, prediction = cv.threshold(prediction, args.threshold, 255, cv.THRESH_BINARY)
    q = RegionQuantities.computeFromImgs(prediction, label, **args_dict)

    print(f"{rdr.__name__:>6}")
    _rdr = rdr(q, **args_dict)
    _rdr = f"{_rdr:.4f}"
    print(f"{_rdr:>6}")
